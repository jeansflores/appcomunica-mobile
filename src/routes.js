import React from 'react'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { createAppContainer, createDrawerNavigator, createStackNavigator, createSwitchNavigator } from 'react-navigation'

import { theme } from './utils'
import AuthStack from './containers/Auth'
import Drawer from './components/Layout/Drawer'
import DrawerButton from './components/Layout/DrawerButton'
import HomeTab from './containers/Home'
import InstitutionTab from './containers/Institution'
import LoadingScreen from './containers/Loading'
import Logo from './components/Layout/Logo'
import MyPromotions from './containers/MyPromotions'
import MyRaffles from './containers/MyRaffles'
import Search from './components/Media/Searcher/Search'
import SearcherScreen from './containers/Searcher'
import UserScreen from './containers/User'

const MainDrawer = createDrawerNavigator({
  home: {
    screen: HomeTab,
    navigationOptions: {
      drawerLabel: 'Início',
      drawerIcon: () => <Icon name="home" size={20} />
    }
  },
  myPromotions: {
    screen: MyPromotions,
    navigationOptions: {
      drawerLabel: 'Minhas Promoções',
      drawerIcon: () => <Icon name="tags" size={20} />
    }
  },
  myRaffles: {
    screen: MyRaffles,
    navigationOptions: {
      drawerLabel: 'Meus Sorteios',
      drawerIcon: () => <Icon name="ticket-alt" size={20} />
    }
  },
  institute: {
    screen: InstitutionTab,
    navigationOptions: {
      drawerLabel: 'Institucional',
      drawerIcon: () => <Icon name="building" size={20} />
    }
  }
}, {
  initialRouteName: 'home',
  drawerWidth: 215,
  contentComponent: Drawer,
  contentOptions: {
    activeTintColor: theme.colors.primary,
    itemsContainerStyle: {
      marginVertical: 0,
    },
    iconContainerStyle: {
      opacity: 1
    }
  },
  navigationOptions: ({ navigation }) => {
    let params = {
      headerTitle: Logo,
      headerBackTitle: null,
      headerStyle: {
        backgroundColor: theme.colors.primary,
        borderBottomColor: 'transparent',
        elevation: 0
      },
      headerLeft: (
        <DrawerButton onPress={() => navigation.toggleDrawer()} />
      )
    }

    if(navigation.state.routes[navigation.state.index].key == 'home') {
      let optionsHeader = {
        headerRight: (
          <Search navigation={navigation}/>
        )
      }

      return Object.assign({}, params, optionsHeader)
    }

    return params
  }
})

const MainStack = createStackNavigator({
  main: {
    screen: MainDrawer
  },
  user: {
    screen: UserScreen
  },
  searcher: {
    screen: SearcherScreen
  },
})

const RootSwitch = createSwitchNavigator({
  app: MainStack,
  auth: AuthStack,
  loading: LoadingScreen,
}, {
  initialRouteName: 'loading'
})

export default createAppContainer(RootSwitch)
