import Reactotron from 'reactotron-react-native'

if (__DEV__) {
  const tron = Reactotron
    .configure({ name: 'CDL Mais Você' })
    .connect()

  tron.clear()

  console.tron = tron
}
