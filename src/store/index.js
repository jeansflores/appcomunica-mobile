import React from 'react'

export * from './actions'

export const Store = React.createContext()

const initialState = {
  ads: {
    events: [],
    news: [],
    promotions: [],
    raffles: [],
  },
  device_token: {},
  institute: {
    accesses: [],
    id: 1,
  },
  myPromotions: [],
  myRaffles: [],
  raffles: [],
  tickets:[],
  user: {},
}

const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_ADS':
      return { ...state, ads: action.payload.ads }
    case 'SET_INSTITUTE':
      return { ...state, institute: action.payload.institute }
    case 'SET_MY_PROMOTIONS':
      return { ...state, myPromotions: action.payload.promotions }
    case 'SET_MY_RAFFLES':
      return { ...state, myRaffles: action.payload.raffles }
    case 'SET_PROMOTIONS':
      return { ...state, promotions: action.payload.promotions }
    case 'SET_RAFFLES':
      return { ...state, raffles: action.payload.raffles}
    case 'SET_TOKEN':
      return { ...state, device_token: action.payload.device_token }
    case 'SET_USER':
      return { ...state, user: action.payload.user }
    default:
      throw new Error('Unexpected action')
  }
}

export const StoreProvider = props => {
  const [state, dispatch] = React.useReducer(reducer, initialState)
  const value = { state, dispatch }

  return <Store.Provider value={value}>{props.children}</Store.Provider>
}
