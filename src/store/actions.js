import Api from '~/api'

export const setInstitute = ({ dispatch }, { institute }) => {
  return dispatch({ type: 'SET_INSTITUTE', payload: { institute } })
}

export const setUser = ({ dispatch }, { user }) => {
  return dispatch({ type: 'SET_USER', payload: { user } })
}

export const setAds = ({ dispatch }, { ads }) => {
  return dispatch({ type: 'SET_ADS', payload: { ads: ads } })
}

export const setToken = ({ dispatch }, { device_token }) => {
  return dispatch({ type: 'SET_TOKEN', payload: { device_token } })
}

export const setPromotions = ({ dispatch }, { promotions }) => {
  return dispatch({ type: 'SET_PROMOTIONS', payload: { promotions } })
}

export const setMyPromotions = ({ dispatch }, { promotions }) => {
  return dispatch({ type: 'SET_MY_PROMOTIONS', payload: { promotions } })
}

export const setMyRaffles = ({ dispatch }, { raffles }) => {
  return dispatch({ type: 'SET_MY_RAFFLES', payload: { raffles } })
}

export const setRaffles = ({ dispatch }, { raffles }) => {
  return dispatch({ type: 'SET_RAFFLES', payload: { raffles } })
}
