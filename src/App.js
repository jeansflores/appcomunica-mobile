import React from 'react'

import '~/config/ReactotronConfig'

import color from 'color'
import Routes from './routes'
import { theme } from '~/utils'
import { StatusBar } from 'react-native'
import { ThemeProvider } from 'styled-components/native'
import { StoreProvider } from './store'

import { YellowBox } from 'react-native'

YellowBox.ignoreWarnings([
  'ViewPagerAndroid has been extracted',
  'Async Storage has been extracted'
])

export default () => {
  const barStyle = (color(theme.colors.primary).isDark ? 'light' : 'dark')
  const backgroundColor = color(theme.colors.primary).darken(0.2)

  return (
    <>
      <StatusBar backgroundColor={backgroundColor} barStyle={`${barStyle}-content`} />
      <StoreProvider>
        <ThemeProvider theme={theme}>
          <Routes />
        </ThemeProvider>
      </StoreProvider>
    </>
  )
}
