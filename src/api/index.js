import Qs from 'qs'
import http from '../services/http'
import JsonApi from 'devour-client'

const Api = new JsonApi({ apiUrl: '' })

Api.axios = http

Api.headers['Accept'] = 'application/vnd.api+json'
Api.headers['Content-Type'] = 'application/vnd.api+json'

Api.define('user', {
  name: '',
  email: '',
  avatar: '',
  password: '',
})

Api.define('category', {
  name: '',
})

Api.define('ad', {
  expire_at: '',
  institute:{
    jsonApi: "hasOne",
    type: "institutes"
  },
  image: '',
  local: '',
  name: ''
})

Api.define('post', {
  title: '',
  subtitle: '',
  content: '',
  image: '',
  publishedAt: '',
  institute: {
    jsonApi: 'hasOne',
    type: 'institutes'
  }
})

Api.define('event', {
  name: '',
  description: '',
  image: '',
  mapUrl: '',
  link: '',
  startAt: '',
  status: '',
  publishedAt: '',
  institute: {
    jsonApi: 'hasOne',
  }
})

Api.define('institute', {
  socialName: '',
  tradeName: '',
  about: '',
  cnpj: '',
  logo: '',
  accesses: [],
  contact: {
    jsonApi: 'hasOne',
  },
  address: {
    jsonApi: 'hasOne',
  },
})

Api.define('contact', {
  email: '',
  phone: '',
  whatsappPhone: '',
})

Api.define('city', {
  name: '',
  state: {
    jsonApi: 'hasOne'
  }
})

Api.define('state', {
  name: '',
  acronym: '',
})

Api.define('address', {
  street: '',
  district: '',
  zipcode: '',
  number: '',
  complement: '',
  mapUrl: '',
  city: {
    jsonApi: 'hasOne',
  }
})

Api.define('company', {
  socialName: '',
  tradeName: '',
  cnpj: '',
  about: '',
  email: '',
  logo: '',
  address: {
    jsonApi: 'hasOne',
  }
})

Api.define('research', {
  name: '',
  description: '',
  regulation: '',
  banner: '',
  logo: '',
  status: '',
  startAt: '',
  endAt: '',
  publishedAt: '',
  totalParticipants: '',
  sponsors: {
    jsonApi: 'hasMany'
  }
})

Api.define('research-participant', {
  votes: {
    jsonApi: 'hasMany'
  },
  research: {
    jsonApi: 'hasOne'
  },
  category: {
    jsonApi: 'hasOne'
  },
  company: {
    jsonApi: 'hasOne'
  }
})

Api.define('vote', {
  rating: '',
  user: {
    jsonApi: 'hasOne',
    type: 'users'
  },
  researchParticipant: {
    jsonApi: 'hasOne',
    type: 'research_participants'
  },
})

Api.define('raffle', {
  amountAvailable: '',
  description: '',
  image: '',
  name: '',
  publishedAt: '',
  raffledAt: '',
  regulation: '',
  remainingVotes: '',
  single: '',
  status: '',
  institute: {
    jsonApi: 'hasOne',
  },
  company: {
    jsonApi: 'hasOne',
  },
  tickets: {
    jsonApi: 'hasMany',
  }
})

Api.define('ticket', {
  raffled: '',
  token: '',
  userId: '',
  raffleId: '',
  user: {
    jsonApi: 'hasOne',
    type: 'users'
  },
  raffle: {
    jsonApi: 'hasOne',
    type: 'raffles'
  },
})

Api.define('promotion', {
  name: '',
  amountAvailable: '',
  status: '',
  description: '',
  image: '',
  expiresAt: '',
  publishedAt: '',
  hasCoupons: '',
  remainingVotes: '',
  institute: {
    jsonApi: 'hasOne',
  },
  company: {
    jsonApi: 'hasOne',
  },
  coupons: {
    jsonApi: 'hasMany',
  }
})

Api.define('coupon', {
  token: '',
  userId: '',
  promotionId: '',
  user: {
    jsonApi: 'hasOne',
    type: 'users'
  },
  promotion: {
    jsonApi: 'hasOne',
    type: 'promotions'
  },
})

Api.define('item', {
  name: '',
  description: '',
  image: '',
  group: '',
  institute: {
    jsonApi: 'hasOne',
    type: 'institutes'
  },
})

Api.define('sponsor', {
  name: '',
  logo: '',
  research: {
    jsonApi: 'hasOne',
  }
})

export const researchApi = {
  fetchOpenedByUser(params) {
    return http.get('researches/show-by-user', {
      params,
      paramsSerializer: params => Qs.stringify(
        params,
        { arrayFormat: 'brackets', encode: false }
      )
    })
  },
  fetchProgressByUser(params) {
    return http.get('researches/progress-by-user', {
      params,
      paramsSerializer: params => Qs.stringify(
        params,
        { arrayFormat: 'brackets', encode: false }
      )
    })
  }
}

export const raffleApi = {
  remainingVotesByUser(params) {
    return http.get('raffles/remaining-votes-by-user', { params })
  }
}

export const promotionApi = {
  remainingVotesByUser(params) {
    return http.get('promotions/remaining-votes-by-user', { params })
  }
}

export const authApi = {
  me() {
    return Api.runMiddleware({
      url: 'me',
      method: 'GET',
      params: {},
      data: {},
      model: 'user'
    })
  },
  register(data) {
    return http.post('auth/register', { registration: data })
  },
  token({ email, password, google_id, facebook_id, device_token }) {
    credentials = { email: email, password: password, device_token: device_token }

    if (typeof google_id !== 'undefined' && google_id != null) {
      credentials = { ...credentials, google_id: google_id}
    } else if (typeof facebook_id !== 'undefined' && facebook_id != null) {
      credentials = { ...credentials, facebook_id: facebook_id}
    }

    return http.post('auth/token', credentials)
  },
  resetToken(email) {
    return http.post('auth/password/forgot', { email })
  },
  resetPassword(payload) {
    return http.post('auth/password/reset', payload)
  }
}

export default Api
