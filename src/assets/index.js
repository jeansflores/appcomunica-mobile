const images = {
  logo: require('./img/logo.png'),
  logoG: require('./img/logo_g.png'),
}

export {
  images,
}
