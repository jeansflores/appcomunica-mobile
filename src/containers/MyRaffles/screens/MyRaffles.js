import { FlatList } from 'react-native'
import { Store, setMyRaffles } from '~/store'
import { withNavigation } from 'react-navigation'
import AdList from '~/components/AdList'
import Api, { raffleApi } from '~/api'
import ListEmpty from '~/components/ListEmpty'
import LoadIndicator from '~/components/LoadIndicator'
import Page from '~/components/Layout/Page'
import Preview from '~/components/Media/MyRaffles/Preview'
import React, { useState, useEffect, useContext } from 'react'
import Row from '~/components/Layout/Row'

export default withNavigation(({ navigation }) => {
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [isLastPage, setIsLastPage] = useState(false)
  const [page, setPage] = useState(1)
  const { state, dispatch } = useContext(Store)

  useEffect(() => {
    navigation.addListener('willFocus', () => {
      handleFetchItems()
    })

    handleFetchItems()
  }, [])

  const handleFetchItems = () => {
    Api.findAll('raffle', {
      sort: '-id',
      filter: {
        institute_id: state.institute.id,
        "tickets.user_id": state.user.id
      },
      include: 'tickets'
    })
    .then(({ data }) => {
      if (data.length) {
        data.map(defineTicket)
        setMyRaffles({ state, dispatch }, { raffles: data })
      } else {
        setIsLastPage(true)
      }
    })

    setPage(1)
  }

  const handleLoadMore = () => {
    if (isLastPage && isRefreshing) return null

    Api.findAll('raffle', {
      sort: '-id',
      page: { number: page + 1 },
      filter: {
        institute_id: state.institute.id,
        "tickets.user_id": state.user.id
      },
      include: 'tickets'
    })
    .then(({ data }) => {
      if (data.length) {
        data.map(defineTicket)
        setMyRaffles({ state, dispatch }, { raffles: state.myRaffles.concat(data) })
        setPage(page + 1)
        setIsLastPage(false)
      } else {
        setIsLastPage(true)
      }
    })
  }

  const defineTicket = (raffle) => {
    const selected = raffle.tickets.filter((ticket) => {
      return state.user.id == ticket.userId && raffle.id == ticket.raffleId
    })[0]

    raffle.ticket = typeof selected === 'undefined' ? null : selected
    delete raffle.tickets

    raffleApi.remainingVotesByUser({ user_id: state.user.id, raffle_id: raffle.id })
      .then(({ data }) => {
        raffle.remainingVotes = data
      })

    return raffle
  }

  const renderFooter = () => {
    if (isLastPage) return null
    return <LoadIndicator />
  }

  const renderListEmpty = () => {
    return isLastPage && (
      <ListEmpty message="VOCÊ NÃO ESTÁ PARTICIPANDO DE NENHUM SORTEIO" />
    )
  }

  return (
    <Page>
      <Row>
        <FlatList
          data={state.myRaffles}
          keyExtractor={item => item.id}
          onRefresh={handleFetchItems}
          refreshing={isRefreshing}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={1}
          ListFooterComponent={renderFooter}
          ListEmptyComponent={renderListEmpty}
          renderItem={({ item, index }) => {
            return (
              <>
                <Preview
                  key={item.id}
                  raffle={item}
                  onPress={
                    () => navigation.navigate('view-raffle', { raffle: item })
                  }
                />
              </>
            )
          }}
        />
      </Row>
    </Page>
  )
})
