import MyRaffles from '~/components/Media/MyRaffles'
import React from 'react'

const ShowMyRaffles = ({ navigation }) => {
  const raffle = navigation.getParam('raffle')

  return (
    <MyRaffles raffle={raffle} ticket={raffle.ticket} navigation={navigation} />
  )
}

export default ShowMyRaffles
