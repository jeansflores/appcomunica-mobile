import { createStackNavigator } from 'react-navigation'
import { theme, colors } from '~/utils'
import MyRaffles from './screens/MyRaffles'
import MyRaffleScreen from './screens/ShowMyRaffles';

export default createStackNavigator({
  'my-raffles': {
    navigationOptions: {
      header: null,
      headerBackTitle: null
    },
    screen: createStackNavigator({
      list: {
        screen: MyRaffles,
        navigationOptions: {
          headerTintColor: colors.white,
          headerStyle: {
            backgroundColor: theme.colors.primary,
          },
          title: `Meus Sorteios`
        },
      },
      'view-raffle': {
        screen: MyRaffleScreen,
        navigationOptions: {
          title: 'Sorteio',
          headerStyle: {
            backgroundColor: theme.colors.secondary,
          },
        },
      }
    }),
  },
})
