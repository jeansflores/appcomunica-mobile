import { createSwitchNavigator } from 'react-navigation'
import { theme } from '~/utils'
import Icon from 'react-native-vector-icons/FontAwesome5'
import React from 'react'
import { View, Text } from 'react-native'
import Search from '~/components/Media/Searcher/Search'
import Filter from '~/components/Media/Searcher/Filter'

import ResultScreen from './screens/Result'
import RaffleRulesScreen from './screens/RaffleRules'
import RaffleScreen from './screens/Raffle'
import PromotionScreen from './screens/Promotion'
import PostScreen from './screens/Post'
import EventScreen from './screens/Event'

export default createSwitchNavigator({
  'searcher-result': {
    screen: ResultScreen
  },
  'searcher-raffle': {
    screen: RaffleScreen
  },
  rules: {
    screen: RaffleRulesScreen,
    navigationOptions: {
      title: 'Regulamento'
    }
  },
  'searcher-promotion': {
    screen: PromotionScreen
  },
  'searcher-post': {
    screen: PostScreen
  },
  'searcher-event': {
    screen: EventScreen
  },
}, {
  initialRouteName: 'searcher-result',
  navigationOptions: ({ navigation }) => {
    let params = {
      headerStyle: { backgroundColor: theme.colors.primary },
      headerTitleStyle: { color: theme.colors.surface },
      headerTintColor: theme.colors.surface,
    }

    let paramsOptions = {}

    if (navigation.state.index == 0) {
      paramsOptions = {
        title: navigation.getParam('search'),
        headerLeft: (
          <Icon
            style={{color: "#FFF", paddingHorizontal: 15, paddingVertical: 15}}
            onPress={() => {
              navigation.navigate('home')
            }}
            name="arrow-left"
            size={20} />
        ),
        headerTitle: (
          <View style={{flex:1, flexDirection: 'row'}}>
            <View style={{flex: 0.8}}>
              <Text
                style={{color: "#FFF", fontWeight: 'bold', fontSize: 18}}
                ellipsizeMode='tail'
                numberOfLines={1}
              >
                {navigation.getParam('search')}
              </Text>
            </View>
          </View>
        ),
        headerRight: (
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Search navigation={navigation}/>
            {navigation.getParam('category') !== 'event' && navigation.getParam('category') !== 'post' && (
              <Filter navigation={navigation}/>
            )}
          </View>
        )
      }
    } else {
      paramsOptions = {
        headerLeft: (
          <Icon
            style={{color: "#FFF", paddingHorizontal: 15, paddingVertical: 15}}
            onPress={() => {
              navigation.navigate(
                {
                  routeName: 'searcher-result',
                  params: {
                    search: navigation.getParam('search'),
                    category: navigation.getParam('category'),
                    filterClosed: navigation.getParam('filterClosed')
                  },
                  key: navigation.getParam('search') + '-' + navigation.getParam('category') + '-' + navigation.getParam('filterClosed')
                }
              )
            }}
            name="arrow-left"
            size={20} />
        ),
      }
    }

    return Object.assign({}, params, paramsOptions)
  }
})
