import { FlatList } from 'react-native'
import { theme } from '~/utils'
import { withNavigation } from 'react-navigation'
import Api, { raffleApi } from '~/api'
import Card from '~/components/Media/Searcher/Card'
import ListEmpty from '~/components/ListEmpty'
import LoadIndicator from '~/components/LoadIndicator'
import Page from '~/components/Layout/Page'
import React, { useEffect, useState, useContext } from 'react'
import Row from '~/components/Layout/Row'
import { Store} from '~/store'

const Result = ({ navigation }) => {
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [isLastPage, setIsLastPage] = useState(false)
  const [page, setPage] = useState(1)
  const [items, setItems] = useState([])
  const { state, dispatch } = useContext(Store)

  useEffect(() => {
    navigation.addListener('action', () => {
      handleFetchItems()
    })
    handleFetchItems()
  }, [])

  const handleFetchItems = () => {
    let include = ''
    let status = navigation.getParam('filterClosed') ? 'opened,closed' : 'opened'

    setIsRefreshing(true)

    switch(navigation.getParam('category')) {
      case 'raffle':
        include = 'tickets'
        break;
      case 'promotion':
        include = 'company,coupons'
        break;
      case 'event':
        status = 'opened'
        break;
      case 'post':
        status = 'published'
        break;
    }

    Api.findAll(navigation.getParam('category'), {
      sort: '-id',
      filter: {
        institute_id: state.institute.id,
        name: navigation.getParam('search'),
        status: status
      },
      include: include
    })
    .then(({ data }) => {
      if (data.length) {
        switch(navigation.getParam('category')) {
          case 'raffle':
            data.map(defineTicket)
            break;
          case 'promotion':
            data.map(defineCoupon)
            break;
        }

        setItems(data)
      } else {
        setIsLastPage(true)
      }
    })

    setIsRefreshing(false)
    setPage(1)
  }

  const handleLoadMore = () => {
    if (isLastPage && isRefreshing) return null

    let include = ''
    let status = navigation.getParam('filterClosed') ? 'opened,closed' : 'opened'

    switch(navigation.getParam('category')) {
      case 'raffle':
        include = 'tickets'
        break;
      case 'promotion':
        include = 'company,coupons'
        break;
      case 'event':
        status = 'opened'
        break;
      case 'post':
        status = 'published'
        break;
    }

    Api.findAll(navigation.getParam('category'), {
      sort: '-id',
      page: { number: page + 1 },
      filter: {
        institute_id: state.institute.id,
        name: navigation.getParam('search'),
        status: status
      },
      include: include
    })
    .then(({ data }) => {
      if (data.length) {
        switch(navigation.getParam('category')) {
          case 'raffle':
            data.map(defineTicket)
            break;
          case 'promotion':
            data.map(defineCoupon)
            break;
        }
        setItems([...items, ...data])
        setPage(page + 1)
        setIsLastPage(false)
      } else {
        setIsLastPage(true)
      }
    })
  }

  const defineTicket = (raffle) => {
    const selected = raffle.tickets.filter((ticket) => {
      return state.user.id == ticket.userId && raffle.id == ticket.raffleId
    })[0]

    raffle.ticket = typeof selected === 'undefined' ? null : selected
    delete raffle.tickets

    raffleApi.remainingVotesByUser({ user_id: state.user.id, raffle_id: raffle.id })
      .then(({ data }) => {
        raffle.remainingVotes = data
      })

    return raffle
  }

  const defineCoupon = (promotion) => {
    const selected = promotion.coupons.filter((coupon) => {
      return state.user.id == coupon.userId && promotion.id == coupon.promotionId
    })[0]

    promotion.coupon = typeof selected === 'undefined' ? null : selected
    delete promotion.coupons

    return promotion
  }

  const renderFooter = () => {
    if (isLastPage) return null
    return <LoadIndicator />
  }

  const renderListEmpty = () => {
    return isLastPage && (
      <ListEmpty message="NENHUM RESULTADO ENCONTRADO" />
    )
  }

  return (
    <Page>
      <Row>
        <FlatList
          data={items}
          keyExtractor={item => item.id}
          onRefresh={handleFetchItems}
          refreshing={isRefreshing}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={1}
          ListFooterComponent={renderFooter}
          ListEmptyComponent={renderListEmpty}
          renderItem={({ item }) => {
            return (
              <>
                <Card
                  key={item.id}
                  title={item.name || item.title}
                  category={navigation.getParam('category')}
                  status={item.status}
                  image={item.image}
                  onPress={
                    () => navigation.navigate('searcher-' + navigation.getParam('category'), { item })
                  }
                />
              </>
            )
          }}
        />
      </Row>
    </Page>
  )
}

export default withNavigation(Result)
