import React from 'react'
import Post from '~/components/Media/Post'

const ShowNews = ({ navigation }) => {
  const post = navigation.getParam('item')

  return <Post post={post} />
}

export default ShowNews
