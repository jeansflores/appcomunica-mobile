import { ScrollView } from 'react-native'
import { Store, setRaffles, setMyRaffles } from '~/store'
import Api from '~/api'
import Html from 'react-native-render-html'
import Page from '~/components/Layout/Page'
import React, { useState, useContext } from 'react'
import Row from '~/components/Layout/Row'
import styled from 'styled-components/native'

const Button = styled.TouchableOpacity`
  align-items: center;
  align-self: center;
  background-color: ${props => props.theme.colors.secondary};
  border-radius: 20;
  elevation: 1;
  height: 32;
  justify-content: center;
  padding-horizontal: 5;
  padding-vertical: 5;
  shadow-color: ${props => props.theme.colors.text};
  shadow-offset: 0px 3px;
  shadow-opacity: 0.25;
  shadow-radius: 5;
  width: 150;
`

const ButtonLabel = styled.Text`
  font-weight: bold;
`

const RaffleRules = (({ navigation }) => {
  const [raffle, setRaffle] = useState(navigation.getParam('raffle'))
  const [isDisabled, setIsDisabled] = useState(false)
  const { state, dispatch } = useContext(Store)

  const handleParticipate = async (raffle) => {
    const { data: ticket } = await Api.create('ticket', {
      user: { id: state.user.id },
      raffle: { id: raffle.id },
    })

    raffle.ticket = ticket

    await Api.find('raffle', ticket.raffleId).then(({ data })=> raffle.status = data.status )

    setMyRaffles({ state, dispatch }, { raffle })
    setRaffles({ state, dispatch }, { raffles: state.raffles })
    setRaffle(raffle)
  }

  return (
    <Page style={ { backgroundColor: '#fff', marginHorizontal: 5, marginVertical: 5 }}>
      <ScrollView>
        <Row>
          <Html html={raffle.regulation} />
          { !raffle.ticket &&
            <Button
              style={{ marginTop: 20, marginBottom: 10 }}
              activeOpacity={0.7}
              disabled={isDisabled}
              onPress={async () => {
                setIsDisabled(true)
                await handleParticipate(raffle)
                setIsDisabled(false)

                navigation.navigate('searcher-raffle',
                  { item: raffle }
                )
              }}
            >
              <ButtonLabel>Participar</ButtonLabel>
            </Button>
          }
        </Row>
      </ScrollView>
    </Page>
  )
})

export default RaffleRules
