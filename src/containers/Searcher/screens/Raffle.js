import Raffle from '~/components/Media/Raffle'
import React from 'react'

const ShowRaffle = ({ navigation }) => {
  const raffle = navigation.getParam('item')

  return (
    <Raffle raffle={raffle} ticket={raffle.ticket} navigation={navigation} />
  )
}

export default ShowRaffle
