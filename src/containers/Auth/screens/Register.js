import { Dimensions, Alert } from 'react-native'
import { Auth } from '~/services'
import { images } from '~/assets'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Store, setUser, setInstitute } from '~/store'
import { theme } from '~/utils'
import { withNavigation } from 'react-navigation'
import Api from '~/api'
import React, { useState, useContext } from 'react'
import styled from 'styled-components/native'

const { height } = Dimensions.get('screen')

const Container = styled.View`
  margin-top: ${height / 7};
  justify-content: center;
`

const LogoContainer = styled.View`
  flex-direction: row;
  justify-content: center;
  margin-bottom: 30;
`

const LogoImage = styled.Image`
  width: 80%;
  aspect-ratio: 1.5;
`

const Form = styled.View`
  width: 80%;
  align-self: center;
`

const TextField = styled.TextInput`
  height: 38;
  padding-horizontal: 20;
  background-color: ${props => props.theme.colors.surface};
  border-radius: 3;
  margin-bottom: 10;
`

const SubmitButton = styled.TouchableOpacity`
  background-color: ${props => props.theme.colors.accent};
  align-items: center;
  width: 100%;
  height: 38;
  border-radius: 3;
  justify-content: center;
  align-self: center;
`

const SubmitButtonLabel = styled.Text`
  color: ${props => props.theme.colors.surface};
  font-weight: bold;
`

const RegisterLabel = styled.Text`
  color: ${props => props.theme.colors.surface};
  align-self: center;
  font-size: 12;
  margin-top: 10;
`

const Link = styled.Text`
  font-weight: bold;
  color: cyan;
`

const LoadingIndicator = styled.ActivityIndicator``

const Register = ({ navigation }) => {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [loading, setLoading] = useState(false)
  const { state, dispatch } = useContext(Store)
  const { device_token } = state

  const handleRegister = () => {
    Auth.register({ name, email, password }).then(() => {
      Auth.loginWithCredentials({ email, password, device_token }).then(() => {
        Promise.all([Auth.fetchUser(), Api.find('institute', state.institute.id)]).then(values => {
          setUser({ state, dispatch }, { user: values[0].data })
          setInstitute({ state, dispatch }, { institute: values[1].data })
          setLoading(false)

          navigation.navigate('app')
        })
      })
    }).catch(error => {
      setLoading(false)

      Alert.alert(error.response.data.errors[0].message)
    })
  }

  return (
    <KeyboardAwareScrollView style={{ backgroundColor: '#143a85' }}>
      <Container>
        <LogoContainer>
          <LogoImage source={images.logo} resizeMode="contain" />
        </LogoContainer>

        <Form>
          <TextField
            placeholder="Nome"
            value={name}
            onChangeText={text => setName(text)}
          />

          <TextField
            placeholder="E-mail"
            value={email}
            autoCapitalize="none"
            onChangeText={text => setEmail(text)}
          />

          <TextField
            placeholder="Senha"
            value={password}
            onChangeText={text => setPassword(text)}
            textContentType="password"
            secureTextEntry
          />

          <SubmitButton
            activeOpacity={0.9}
            disabled={loading}
            onPress={() => {
              setLoading(true)
              handleRegister()
            }}
          >

            { !loading ? (
              <SubmitButtonLabel>Registrar</SubmitButtonLabel>
            ) : (
              <LoadingIndicator color={theme.colors.surface} />
            )}
          </SubmitButton>
        </Form>

        <RegisterLabel>
          Já possui uma conta?
          <Link onPress={() => navigation.navigate('login')}> Entre aqui</Link>
        </RegisterLabel>
      </Container>
    </KeyboardAwareScrollView>
  )
}

export default withNavigation(Register)
