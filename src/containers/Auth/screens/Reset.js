import { Dimensions, Alert } from 'react-native'
import { Auth } from '~/services'
import { images } from '~/assets'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Store, setUser, setInstitute } from '~/store'
import { theme } from '~/utils'
import { withNavigation } from 'react-navigation'
import Api from '~/api'
import React, { useState, useContext } from 'react'
import styled from 'styled-components/native'

const { height } = Dimensions.get('screen')

const Container = styled.View`
  margin-top: ${height / 4};
  justify-content: center;
`

const LogoContainer = styled.View`
  flex-direction: row;
  justify-content: center;
  margin-horizontal: 10;
  margin-bottom: 10;
`

const LogoImage = styled.Image`
  width: 80%;
  aspect-ratio: 1.5;
`
const Form = styled.View`
  width: 80%;
  align-self: center;
`

const TextField = styled.TextInput`
  height: 38;
  padding-horizontal: 20;
  background-color: ${props => props.theme.colors.surface};
  border-radius: 3;
  margin-bottom: 10;
`

const SubmitButton = styled.TouchableOpacity`
  background-color: ${props => props.theme.colors.accent};
  align-items: center;
  width: 100%;
  height: 38;
  border-radius: 3;
  justify-content: center;
  align-self: center;
`

const SubmitButtonLabel = styled.Text `
  color: ${props => props.theme.colors.surface};
  font-weight: bold;
`

const LoginLabel = styled.Text`
  color: ${props => props.theme.colors.surface};
  align-self: center;
  font-size: 12;
  margin-top: 10;
`

const Link = styled.Text`
  font-weight: bold;
  color: cyan;
`

const LoadingIndicator = styled.ActivityIndicator``


const Login = ({ navigation }) => {
  const [email, setEmail] = useState('')
  const [token, setToken] = useState('')
  const [password, setPassword] = useState('')
  const [tokenSent, setTokenSent] = useState(false)
  const [loading, setLoading] = useState(false)
  const { state, dispatch } = useContext(Store)

  const handleLogin = () => {
    Auth.loginWithCredentials({ email, password })
      .then(async () => {
        Promise.all([
          Auth.fetchUser(),
          Api.find('institute', state.institute.id)
        ]).then(values => {
          setUser({ state, dispatch }, { user: values[0].data })
          setInstitute({ state, dispatch }, { institute: values[1].data })
          setLoading(false)

          navigation.navigate('app')
        })
      })
  }

  const handleResetToken = () => {
    Auth.resetToken(email)
      .then(() => {
        setLoading(false)
        setTokenSent(true)

        Alert.alert("Código de alteração de senha enviado " + email)
      })
      .catch(error => {
        if (error.response) {
          setLoading(false)

          Alert.alert(error.response.data.errors[0].message)
        }
      })
  }

  const handleResetPassword = () => {
    Auth.resetPassword({ token, password })
      .then(handleLogin)
      .catch(error => {
        if (error.response) {
          setLoading(false)

          Alert.alert(error.response.data.errors[0].message)
        }
      })
  }

  return (
    <KeyboardAwareScrollView style={{ backgroundColor: '#143a85' }}>
      <Container>
        <LogoContainer>
          <LogoImage source={images.logo} resizeMode="contain" />
        </LogoContainer>

        <Form>
          { tokenSent ? (
            <>
              <TextField
                placeholder="Código"
                value={token}
                onChangeText={text => setToken(text)}
              />
              <TextField
                placeholder="Senha"
                value={password}
                onChangeText={text => setPassword(text)}
                textContentType="password"
                secureTextEntry
              />
            </>
          ) : (
            <TextField
              placeholder="E-mail"
              value={email}
              autoCapitalize="none"
              onChangeText={text => setEmail(text)}
            />
          )}

          <SubmitButton
            activeOpacity={0.9}
            disabled={loading}
            onPress={() => {
              setLoading(true)
              tokenSent ? handleResetPassword() : handleResetToken()
            }}
          >

            { !loading ? (
              <SubmitButtonLabel>
                {tokenSent ? 'Alterar senha' : 'Recuperar senha'}
              </SubmitButtonLabel>
            ) : (
              <LoadingIndicator color={theme.colors.surface} />
            )}
          </SubmitButton>
        </Form>

        { tokenSent ? (
          <LoginLabel>
            Não recebeu o código?
            <Link onPress={() => setTokenSent(false)}> Enviar novamente</Link>
          </LoginLabel>
        ) : (
          <LoginLabel>
            Já possui o código?
            <Link onPress={() => setTokenSent(true)}> Inserir o código</Link>
          </LoginLabel>
        )}

        <LoginLabel style={{ marginTop: 5 }}>
          Lembrou sua senha?
          <Link onPress={() => navigation.navigate('login')}> Voltar para o login</Link>
        </LoginLabel>
      </Container>
    </KeyboardAwareScrollView>
  )
}

export default withNavigation(Login)
