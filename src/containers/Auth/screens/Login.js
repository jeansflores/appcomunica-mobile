import { Auth } from '~/services'
import { colors } from '~/utils'
import { Dimensions, Platform, Alert, Image } from 'react-native'
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin'
import { images } from '~/assets'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Store, setUser, setInstitute } from '~/store'
import { withNavigation } from 'react-navigation'
import Api from '~/api'
import Icon from 'react-native-vector-icons/FontAwesome5'
import LoadingModal from '~/components/LoadingModal'
import React, { useState, useContext, useEffect } from 'react'
import styled from 'styled-components/native'

import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginButton
} from 'react-native-fbsdk'

const { height } = Dimensions.get('screen')

const Container = styled.View`
  margin-top: ${height / 13};
  justify-content: center;
`

const LogoContainer = styled.View`
  flex-direction: row;
  justify-content: center;
  margin-horizontal: 10;
  margin-bottom: 10;
`

const LogoImage = styled.Image`
  width: 80%;
  aspect-ratio: 1.5;
`

const Form = styled.View`
  margin-top: 10;
  margin-bottom: 5;
  width: 80%;
  align-self: center;
`

const TextField = styled.TextInput`
  height: 38;
  padding-horizontal: 20;
  background-color: ${props => props.theme.colors.surface};
  border-radius: 3;
  margin-bottom: 3;
  text-align: center;
`

const Button = styled.TouchableOpacity`
  background-color: ${props => props.theme.colors.accent};
  align-items: center;
  width: 100%;
  height: 35;
  border-radius: 3;
  justify-content: center;
  align-self: center;
`

const ButtonRegister = styled.TouchableOpacity`
  background-color: ${props => props.theme.colors.accent};
  align-items: center;
  width: 100%;
  height: 35;
  border-radius: 3;
  justify-content: center;
  align-self: center;
  margin-top: 5;
  width: 80%;
`

const BoxSocial = styled.View`
  margin-top: 5;
  margin-bottom: 10;
  width: 80%;
  align-self: center;
  align-items: center;
`

const ButtonSocial = styled.TouchableOpacity`
  width: 100%;
  height: 35;
  border-radius: 3;
`

const LoginButtonFB = styled(LoginButton)`
  flex-direction: row;
  align-items: center;
  width: 100%;
  height: 35;
  justify-content: center;
  margin-bottom: 5;
`

const ButtonSocialLabel = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  width: 100%;
  height: 35;
  border-radius: 50;
  justify-content: center;
  margin-vertical: 5;
`

const ButtonLabel = styled.Text `
  color: ${props => props.theme.colors.surface};
  font-weight: bold;
`

const LoginLabel = styled.Text`
  color: ${props => props.theme.colors.surface};
  align-self: center;
  font-size: 12;
  margin-top: 10;
`

const Link = styled.Text`
  font-weight: bold;
  color: cyan;
`

const MessageLogin = styled.Text`
  font-weight: bold;
  align-self: center;
  color: ${colors.white};
`

const BoxIcon = styled.View`
  position: absolute;
  left: 8;
`

const Login = ({ navigation }) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [loading, setLoading] = useState(false)
  const { state, dispatch } = useContext(Store)
  const { device_token } = state

  useEffect(() => {
    GoogleSignin.configure({
      webClientId: '359534332983-s8kn43db9k2fadr95ofecfqt2oqfnm44.apps.googleusercontent.com',
      iosClientId: '359534332983-fildkcm78iufpq56bkeipu83p6bo6md6.apps.googleusercontent.com',
      offlineAccess: true
    })
  }, [])

  const signInGoogle = async () => {
    try {
      await GoogleSignin.hasPlayServices()

      const { user } = await GoogleSignin.signIn()

      handleLoginSocial({
        name: user.name,
        email: user.email,
        avatar: user.photo,
        google_id: user.id,
        device_token
      })
    } catch (error) {
      setLoading(false)
    }
  }

  const FBLoginCallback = (error, user) => {
    if (error) {
      setLoading(false)
    } else {
      if (user.email == null) {
        setLoading(false)
        return Alert.alert('Ops, algo deu errado!', 'E-mail obrigatório')
      }
      handleLoginSocial({
        name: user.name,
        email: user.email,
        avatar: user.picture.data.url,
        facebook_id: user.id,
        device_token
      })
    }
  }

  const handleLoginSocial = (userInfo) => {
    Auth.register({ ...userInfo }).then(() => {
      Auth.loginWithCredentials({ ...userInfo }).then(() => {
        Promise.all([Auth.fetchUser(), Api.find('institute', state.institute.id)]).then(values => {
          setUser({ state, dispatch }, { user: values[0].data })
          setInstitute({ state, dispatch }, { institute: values[1].data })
          setLoading(false)

          navigation.navigate('app')
        })
      })
    }).catch(error => {
      setLoading(false)

      Alert.alert(error.response.data.errors[0].message)
    })
  }

  const handleLogin = () => {
    Auth.loginWithCredentials({ email, password, device_token })
      .then(async () => {
        Promise.all([Auth.fetchUser(), Api.find('institute', state.institute.id)]).then(values => {
          setUser({ state, dispatch }, { user: values[0].data })
          setInstitute({ state, dispatch }, { institute: values[1].data })
          setLoading(false)

          navigation.navigate('app')
        })
      })
      .catch(error => {
        if (error.response) {
          setLoading(false)

          Alert.alert(error.response.data.errors[0].message)
        }
      })
  }

  return (
    <>
      <KeyboardAwareScrollView style={{ backgroundColor: '#143a85' }}>
        <Container>
          <LogoContainer>
            <LogoImage source={images.logo} resizeMode="contain" />
          </LogoContainer>
          <BoxSocial>
            <LoginButtonFB
              readPermissions={["email"]}
              loginBehaviorAndroid='web_only'
              loginBehaviorIOS='browser'
              onLoginFinished={
                (error, result) => {
                  if (error) {
                    console.log("login has error: " + result.error);
                  } else if (result.isCancelled) {
                    console.log("login is cancelled.");
                  } else {
                    setLoading(true)

                    AccessToken.getCurrentAccessToken()
                      .then(data => {
                        const infoRequest = new GraphRequest(
                          '/me?fields=id,name,email,picture.type(large)',
                          null,
                          FBLoginCallback
                        )

                        new GraphRequestManager().addRequest(infoRequest).start()
                      })
                      .catch (error => {
                        setLoading(false)
                      })
                  }
                }
              }
              onLogoutFinished={() => console.log("logout.")}
            />
            <ButtonSocial
              activeOpacity={1}
              disabled={loading}
              onPress={() => {
                setLoading(true)
                signInGoogle()
              }}
              style={{backgroundColor: colors.white}}
            >
              <ButtonSocialLabel>
                <BoxIcon>
                  <Image style={{height: 18, width: 18}} source={images.logoG} resizeMode="contain" />
                </BoxIcon>
                <ButtonLabel style={{color: colors.grey}} >Continuar com o Google</ButtonLabel>
              </ButtonSocialLabel>
            </ButtonSocial>
          </BoxSocial>
          <MessageLogin>ou use seu e-mail</MessageLogin>
          <Form>
            <TextField
              placeholder="E-mail"
              value={email}
              autoCapitalize="none"
              onChangeText={text => setEmail(text)}
            />

            <TextField
              placeholder="Senha"
              value={password}
              onChangeText={text => setPassword(text)}
              textContentType="password"
              secureTextEntry
            />

            <Button
              activeOpacity={0.9}
              disabled={loading}
              onPress={() => {
                setLoading(true)
                handleLogin()
              }}
            >
              <ButtonLabel>Entrar</ButtonLabel>
            </Button>
          </Form>
          <ButtonRegister
            activeOpacity={0.9}
            disabled={loading}
            onPress={() => navigation.navigate('register')}
          >
            <ButtonLabel>Registrar</ButtonLabel>
          </ButtonRegister>
          <LoginLabel style={{ marginTop: 5 }}>
            Esqueceu sua senha?
            <Link onPress={() => navigation.navigate('reset')}> Solicite outra aqui</Link>
          </LoginLabel>
        </Container>
      </KeyboardAwareScrollView>
      { loading && <LoadingModal/> }
    </>
  )
}

export default withNavigation(Login)
