import { createStackNavigator } from 'react-navigation'

import LoginScreen from  './screens/Login'
import ResetScreen from  './screens/Reset'
import RegisterScreen from  './screens/Register'

export default createStackNavigator({
  login: {
    screen: LoginScreen
  },
  register: {
    screen: RegisterScreen
  },
  reset: {
    screen: ResetScreen
  },
}, {
  initialRouteName: 'login',
  headerMode: 'none'
})
