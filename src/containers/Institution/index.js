import { createMaterialTopTabNavigator, createStackNavigator } from 'react-navigation'
import { Platform, Dimensions } from 'react-native'
import { theme, colors } from '~/utils'
import AboutScreen from './screens/About'
import ContactScreen from './screens/Contact'
import ItemsScreen from './screens/Items'
import ShowItemScreen from './screens/ShowItem'

const { height } = Dimensions.get('window')

const createTabStack = (screen) => {
  return createStackNavigator({
    default: {
      screen,
      navigationOptions: {
        header: null,
        headerBackTitle: null,
      },
    },
    ['show-item']: {
      screen: ShowItemScreen,
      navigationOptions: {
        headerBackTitle: null,
        headerTintColor: colors.black,
        headerStyle: {
          marginTop: Platform.select({ ios: height <= 667 || height == 736 ? -20 : -45 }),
          backgroundColor: theme.colors.secondary,
        },
      }
    },
  }, {
    navigationOptions: ({ navigation }) => {
      let tabBarVisible = true
      let swipeEnabled = true

      if (navigation.state.index > 0) {
        tabBarVisible = false
        swipeEnabled = false
      }

      return {
        tabBarVisible,
        swipeEnabled,
      }
    }
  })
}

export default createMaterialTopTabNavigator({
  about: {
    screen: AboutScreen,
    navigationOptions: {
      title: 'Sobre',
    }
  },
  items: {
    screen: createTabStack(ItemsScreen),
    navigationOptions: {
      title: 'Produtos e Serviços',
    },
  },
  contact: {
    screen: ContactScreen,
    navigationOptions: {
      title: 'Contato',
    }
  }
}, {
  initialRouteName: 'about',
  tabBarOptions: {
    scrollEnabled: true,
    animationEnabled: true,
    activeTintColor: theme.colors.primary,
    inactiveTintColor: theme.colors.primary,
    style: {
      paddingVertical: 13,
      backgroundColor: theme.colors.primary,
    },
    tabStyle: {
      padding: 0,
      width: 150,
    },
    labelStyle: {
      margin: 0,
      padding: 2,
      fontSize: 12,
      color: 'white',
    },
    indicatorStyle: {
      backgroundColor: theme.colors.secondary,
      height: 3,
    }
  }
})
