import { Store } from '~/store'
import React, { useContext } from 'react'
import Html from 'react-native-render-html'
import styled from 'styled-components/native'

const Container = styled.ScrollView`
  padding-bottom: 10;
  padding-horizontal: 10;
`

const Description = styled.View``

const Banner = styled.Image`
  width: 100%;
  height: 150;
`

export default () => {
  const { state } = useContext(Store)
  const logo = state.institute.logo

  return (
    <>
      <Banner source={ typeof logo === 'string' ? { uri: logo } : logo } resizeMode="cover" />
      <Container contentContainerStyle={{ paddingVertical: 20 }}>
        <Description>
          <Html html={state.institute.about} />
        </Description>
      </Container>
    </>
  )
}
