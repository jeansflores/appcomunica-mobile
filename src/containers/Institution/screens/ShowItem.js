import React, { useEffect } from 'react'
import Item from '~/components/Media/Item'

const ShowItem = ({ navigation }) => {
  const item = navigation.getParam('item')

  useEffect(() => {
    navigation.setParams({
      title: item.group
    })
  }, [])

  return <Item data={item} />
}

ShowItem.navigationOptions = ({ navigation }) => {
  return {
    title: navigation.getParam('title'),
  }
}

export default ShowItem
