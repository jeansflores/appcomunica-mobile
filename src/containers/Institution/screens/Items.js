import { FlatList } from 'react-native'
import { Store } from '~/store'
import { withNavigation } from  'react-navigation'
import Api from '~/api'
import ListEmpty from '~/components/ListEmpty'
import LoadIndicator from '~/components/LoadIndicator'
import Page from '~/components/Layout/Page'
import Preview from '~/components/Media/Item/Preview'
import React, { useState, useEffect, useContext } from 'react'
import Row from '~/components/Layout/Row'

export default withNavigation(({ navigation }) => {
  const [items, setItems] = useState([])
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [isLastPage, setIsLastPage] = useState(false)
  const [page, setPage] = useState(1)
  const { state } = useContext(Store)

  useEffect(() => {
    handleFetchItems()
  }, [])

  const handleFetchItems = () => {
    setIsRefreshing(true)

    Api.findAll('item', {
      sort: '-id',
      filter: { institute_id: state.institute.id }
    })
    .then(({ data }) => {
      if (data.length) {
        setItems(data)
        setIsLastPage(false)
      } else {
        setIsLastPage(true)
      }
    })

    setIsRefreshing(false)
    setPage(1)
  }

  const handleLoadMore = () => {
    if (isLastPage) {
      return
    }

    Api.findAll('item', {
        sort: '-id',
        page: { number: page + 1 },
        filter: { institute_id: state.institute.id }
      })
      .then(({ data }) => {
        if (data.length) {
          setItems([...items, ...data])
          setPage(page + 1)
          setIsLastPage(false)
        } else {
          setIsLastPage(true)
        }
      })
  }

  const renderFooter = () => {
    if(isLastPage) return null

    return <LoadIndicator />
  }

  const renderListEmpty = () => {
    return isLastPage && (
      <ListEmpty message="NO MOMENTO NÃO HÁ PRODUTO OU SERVIÇO DISPONÍVEL" />
    )
  }

  return (
    <Page>
      <Row>
        <FlatList
          data={items}
          keyExtractor={item => item.id}
          onRefresh={handleFetchItems}
          refreshing={isRefreshing}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={1}
          ListFooterComponent={renderFooter}
          ListEmptyComponent={renderListEmpty}
          renderItem={({ item }) =>
            <Preview
              key={item.id}
              item={item}
              onPress={() => navigation.navigate('show-item', { item })}
            />
          }
        />
      </Row>
    </Page>
  )
})
