import Api from '~/api'
import React, { useContext, useEffect, useState } from 'react'
import Icon from 'react-native-vector-icons/FontAwesome5'
import {
  View,
  Text,
  Alert,
  Linking,
  ScrollView,
  StyleSheet,
  ActivityIndicator
} from 'react-native'
import { Store } from '~/store'
import Divider from '~/components/Divider'
import styled from 'styled-components/native'

const styles = StyleSheet.create({
  container: {
    padding: 10
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  divider: {
    marginVertical: 10,
    borderBottomWidth: 1
  }
})

const Button = styled.TouchableOpacity`
  align-items: center;
  align-self: center;
  background-color: ${props => props.theme.colors.secondary};
  border-radius: 10;
  border-radius: 20;
  elevation: 1;
  justify-content: center;
  padding-horizontal: 5;
  padding-vertical: 5;
  shadow-color: ${props => props.theme.colors.text};
  shadow-offset: 0px 3px;
  shadow-opacity: 0.25;
  shadow-radius: 5;
`

const ButtonLabel = styled.Text`
  color: ${props => props.theme.colors.primary};
  font-size: 12;
  font-weight: bold;
`

export default () => {
  const { state } = useContext(Store)
  const [contact, setContact] = useState()
  const [address, setAddress] = useState()

  useEffect(() => {
    Api.find(
      'institute',
      state.institute.id,
      { include: 'contact,address.city.state' }
    ).then(({ data }) => {
      setContact(data.contact)
      setAddress(data.address)
    })
  }, [])

  return contact && address ? (
    <ScrollView style={styles.container}>
      <Text>
        Canais disponibilizados para garantir a satisfação e excelência no atendimento.
      </Text>

      <Divider />

      <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>Telefone</Text>
      <Text style={{ textAlign: 'center' }}>
        <Icon name="phone" size={20} />
        <Text onPress={() => Linking.openURL(`tel://${contact.phone}`)}> {contact.phone}</Text>
      </Text>

      <Text style={{ textAlign: 'center', fontWeight: 'bold', marginTop: 20 }}>WhatsApp</Text>
      <Text style={{ textAlign: 'center' }}>
        <Icon name="whatsapp" size={20} />
        <Text onPress={() => Linking.openURL(`whatsapp://send?phone=+55${contact.whatsappPhone}`).catch(err => {
          Alert.alert(
            'WhatsApp não disponível',
            'Você deseja instalar o WhatsApp agora?',
            [
              { text: 'Não', onPress: () => {} },
              {
                text: 'Sim',
                onPress: () => Linking.openURL('market://details?id=com.whatsapp')
              }
            ]
          )
        })}> {contact.whatsappPhone}</Text>
      </Text>

      <Text style={{ textAlign: 'center', fontWeight: 'bold', marginTop: 20 }}>E-mail</Text>
      <Text style={{ textAlign: 'center' }} onPress={() => Linking.openURL(`mailto:${contact.email}`)}>
        <Icon name="envelope" size={20} /> {contact.email}
      </Text>

      <Text style={{ textAlign: 'center', fontWeight: 'bold', marginTop: 20 }}>Endereço</Text>

      <View>
        <Text style={{ textAlign: 'center' }}>{address.street} - {address.district}</Text>
        <Text style={{ textAlign: 'center' }}>CEP {address.zipcode} - {address.city.name} - {address.city.state.acronym}</Text>

        <Button
          style={{ width: 120, height: 32, marginTop: 10 }}
          activeOpacity={0.7}
          onPress={() => {
            address.mapUrl ? Linking.openURL(address.mapUrl) : null}
          }
        >
          <Text style={{ alignItems: 'center' }}>
            <Icon name="map-marker" /> <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>Abrir mapa</Text>
          </Text>
        </Button>
      </View>
    </ScrollView>
  ) : (
    <ActivityIndicator />
  )
}
