import { createSwitchNavigator } from 'react-navigation'

import EditScreen, { navigationOptions } from './screens/Edit'

export default createSwitchNavigator({
  'user-edit': {
    screen: EditScreen
  },
}, {
    navigationOptions: navigationOptions
  }
)
