import { Alert, Platform } from 'react-native'
import { Store, setUser } from '~/store'
import { theme } from '~/utils'
import { withNavigation } from 'react-navigation'
import Api from '~/api'
import InputLabel from '~/components/InputLabel'
import LoadingModal from '~/components/LoadingModal'
import React, { useState, useContext } from 'react'
import styled from 'styled-components/native'

const Container = styled.ScrollView`
  flex: 1;
  padding-vertical: 10;
  padding-horizontal: 10;
`

const ContainerAvatar = styled.View`
  align-items: center;
  margin-vertical: 10;
`

const Avatar = styled.Image`
  border-radius: ${ Platform.OS === 'ios' ? 60 : 75 };
  width: 120;
  height: 120;
`

const ContainerForm = styled.View`
  margin-vertical: 10;
`

const SubmitButton = styled.TouchableOpacity`
  width: 120;
  height: 32;
  border-radius: 10;
  padding-horizontal: 5;
  padding-vertical: 5;
  margin-horizontal: 10;
  margin-vertical: 10;
  align-items: center;
  align-self: center;
  justify-content: center;
  background-color: ${props => props.theme.colors.secondary};
  border-radius: 20;
  shadow-color: ${props => props.theme.colors.text};
  shadow-opacity: 0.18;
  shadow-radius: 10;
  elevation: 1;
`

const SubmitButtonLabel = styled.Text`
  font-weight: bold;
`

export function navigationOptions({ navigation }) {
  return {
    title: 'Editar Perfil',
    headerStyle: { backgroundColor: theme.colors.primary },
    headerTitleStyle: { color: theme.colors.surface },
    headerTintColor: theme.colors.surface,
  }
}

const Edit = ({ navigation }) => {
  const { state, dispatch } = useContext(Store)
  const [ name, setName ] = useState(state.user.name)
  const [ email, setEmail ] = useState(state.user.email)
  const [ password, setPassword ] = useState('')
  const [ confirmPassword, setConfirmPassword ] = useState('')
  const [ colorConfirmPassword, setColorConfirmPassword ] = useState('red')
  const [loading, setLoading] = useState(false)

  const handleEditUser = () => {
    if (password != confirmPassword) {
      setLoading(false)
      return Alert.alert("Senhas não coincidem!")
    } else if (password.length == 0 && confirmPassword.length > 0) {
      setLoading(false)
      return Alert.alert("Senha obrigatória!")
    } else if (password.length > 0 && confirmPassword.length == 0) {
      setLoading(false)
      return Alert.alert("Por favor, confirma sua nova senha!")
    }

    if (!name.length) {
      setLoading(false)
      return Alert.alert("Nome obrigatório!")
    }

    if (!email.length) {
      setLoading(false)
      return Alert.alert("E-mail obrigatório!")
    }

    let params = { id: state.user.id, name: name, email: email }

    if (password.length) {
      params = { ...params, password: password }
    }

    Api.update('user', params )
    .then(({ data }) => {
      setUser({ state, dispatch }, { user: data })
      setLoading(false)

      navigation.navigate("summary")
    })
  }

  return (
    <>
      <Container>
        <ContainerAvatar>
          { state.user.avatar ? (
            <Avatar source={{ uri: state.user.avatar }} resizeMode="cover" />
          ) : (
            <Avatar source={ require('~/assets/img/default-icon-user.png') } resizeMode="cover" />
          )}
        </ContainerAvatar>
        <ContainerForm>
          <InputLabel
            label="Nome"
            placeholder="Nome"
            value={name}
            onChangeText={text => setName(text)}
          />
          <InputLabel
            label="E-mail"
            placeholder="exemplo@email.com"
            value={email}
            onChangeText={text => setEmail(text)}
          />
          <InputLabel
            label="Senha"
            placeholder="Caso queira alterar sua senha"
            onChangeText={text => setPassword(text)}
            textContentType="password"
            secureTextEntry
          />
          <InputLabel
            label="Confirmar senha"
            colorFocus={colorConfirmPassword}
            placeholder="Confirmar nova senha"
            onChangeText={text => {
              setConfirmPassword(text)
              text == password ? setColorConfirmPassword('green') : setColorConfirmPassword('red')
            }}
            textContentType="password"
            secureTextEntry
          />

          <SubmitButton
            activeOpacity={0.5}
            onPress={() => {
              setLoading(true)
              handleEditUser()
            }}
          >
            <SubmitButtonLabel>Salvar</SubmitButtonLabel>
          </SubmitButton>
        </ContainerForm>
      </Container>
      { loading && <LoadingModal/> }
    </>
  )
}

export default withNavigation(Edit)
