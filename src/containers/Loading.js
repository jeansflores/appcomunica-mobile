import { Auth } from '~/services'
import { Store, setUser, setInstitute, setToken } from '~/store'
import { theme } from '~/utils'
import { withNavigation } from 'react-navigation'
import Api from '~/api'
import LoadIndicator from '~/components/LoadIndicator'
import React, { useEffect, useContext } from 'react'
import styled from 'styled-components/native'
import OneSignal from 'react-native-onesignal'

const Container = styled.View`
  align-items: center;
  background-color: ${props => props.theme.colors.primary};
  bottom: 0;
  flex: 1;
  justify-content: center;
  left: 0;
  right: 0;
  top: 0;
`

export default withNavigation(({ navigation }) => {
  const { state, dispatch } = useContext(Store)

  useEffect(() => {
    initNotification().then()

    Auth.fetchUser().then(({ data }) => {
      setUser({ state, dispatch }, { user: data })
      Api.find('institute', state.institute.id).then(({ data }) => {
        setInstitute({ state, dispatch }, { institute: data })
        navigation.navigate('app')
      })
    }).catch(() => {
      navigation.navigate('auth')
    })
  }, [])

  const initNotification = async () => {
    await OneSignal.init("daabec13-42c9-4d4e-98cd-34b3232a9559")
    await OneSignal.addEventListener('received', () => {})
    await OneSignal.addEventListener('opened', () => {})
    await OneSignal.addEventListener('ids', (device_token) => {
      return setToken({ state, dispatch }, { device_token })
    })

    await OneSignal.inFocusDisplaying(2)

    await OneSignal.configure()
  }

  return (
    <Container>
      <LoadIndicator color={theme.colors.surface} />
    </Container>
  )
})
