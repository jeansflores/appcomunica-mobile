import React from 'react'
import { createStackNavigator, createMaterialTopTabNavigator } from 'react-navigation'
import { theme, colors } from '~/utils'
import { Platform, Dimensions } from 'react-native'
import EventsScreen from './screens/Events'
import FilterScreen from './screens/Filter'
import ModalSearcherReview from '~/components/Media/Review/Searcher'
import NewsScreen from './screens/News'
import PromotionsScreen from './screens/Promotions'
import RafflesScreen from './screens/Raffles'
import RaffleRulesScreen from './screens/RaffleRules'
import ReviewRulesScreen from './screens/ReviewRules'
import ReviewsScreen from './screens/Reviews'
import ReviewsSummaryScreen from './screens/ReviewsSummary'
import ShowEventScreen from './screens/ShowEvent'
import ShowNewsScreen from './screens/ShowNews'
import ShowPromotionScreen from './screens/ShowPromotion'
import ShowRaffleScreen from './screens/ShowRaffle'

const { height } = Dimensions.get('window')

const createTabStack = screen => {
  return createStackNavigator({
    default: {
      screen,
      navigationOptions: {
        header: null,
        headerBackTitle: null,
      }
    },
    post: {
      screen: ShowNewsScreen,
      navigationOptions: {
        title: 'Notícia',
        headerBackTitle: null,
        headerTintColor: colors.black,
        headerStyle: {
          marginTop: Platform.select({ ios: height <= 667 || height == 736 ? -20 : -45 }),
          backgroundColor: theme.colors.secondary,
        },
      }
    },
    promotion: {
      screen: ShowPromotionScreen,
      navigationOptions: {
        title: 'Promoção',
        headerBackTitle: null,
        headerTintColor: colors.black,
        headerStyle: {
          marginTop: Platform.select({ ios: height <= 667 || height == 736 ? -20 : -45 }),
          backgroundColor: theme.colors.secondary,
        },
      }
    },
    event: {
      screen: ShowEventScreen,
      navigationOptions: {
        title: 'Evento',
        headerBackTitle: null,
        headerTintColor: colors.black,
        headerStyle: {
          marginTop: Platform.select({ ios: height <= 667 || height == 736 ? -20 : -45 }),
          backgroundColor: theme.colors.secondary,
        },
      }
    },
  }, {
    navigationOptions: ({ navigation }) => {
      let tabBarVisible = true
      let swipeEnabled = true

      if (navigation.state.index > 0) {
        tabBarVisible = false
        swipeEnabled = false
      }

      return {
        tabBarVisible,
        swipeEnabled,
      }
    }
  })
}

export default createMaterialTopTabNavigator({
  'reviews-summary': {
    screen: createStackNavigator({
      summary: {
        screen: ReviewsSummaryScreen,
        navigationOptions: {
          header: null,
          headerBackTitle: null
        }
      },
      rules: {
        screen: ReviewRulesScreen,
        navigationOptions: {
          title: 'Regulamento',
          headerBackTitle: null,
          headerTintColor: colors.black,
          headerStyle: {
            marginTop: Platform.select({ ios: height <= 667 || height == 736 ? -20 : -45 }),
            backgroundColor: theme.colors.secondary,
          },
        }
      },
      reviews: {
        screen: ReviewsScreen,
        navigationOptions: ({ navigation }) => ({
          title: (
            typeof(navigation.state.params)==='undefined' ||
            typeof(navigation.state.params.title) === 'undefined' ||
            navigation.state.params.title === '' ? 'Empresas': navigation.state.params.title),
          headerBackTitle: null,
          headerTintColor: colors.black,
          headerStyle: {
            marginTop: Platform.select({ ios: height <= 667 || height == 736 ? -20 : -45 }),
            backgroundColor: theme.colors.secondary,
          },
          headerRight: <ModalSearcherReview navigation={navigation} />,
        })
      },
      filter: {
        screen: FilterScreen,
        navigationOptions: {
          title: 'Selecione',
          headerBackTitle: null,
          headerTintColor: colors.black,
          headerStyle: {
            marginTop: Platform.select({ ios: height <= 667 || height == 736 ? -20 : -45 }),
            backgroundColor: theme.colors.secondary,
          },
        }
      },
    }, {
      initialRouteName: 'summary',
      navigationOptions: ({ navigation }) => {
        let tabBarVisible = true
        let swipeEnabled = true

        if (navigation.state.index > 0) {
          tabBarVisible = false
          swipeEnabled = false
        }

        return {
          tabBarVisible,
          swipeEnabled,
        }
      }
    }),
    navigationOptions: {
      title: 'Avalie +'
    }
  },
  raffles: {
    screen: createStackNavigator({
      list: {
        screen: RafflesScreen,
        navigationOptions: {
          header: null,
          headerBackTitle: null
        },
      },
      raffle: {
        screen: ShowRaffleScreen,
        navigationOptions: {
          title: 'Sorteio',
          headerBackTitle: null,
          headerTintColor: colors.black,
          headerStyle: {
            marginTop: Platform.select({ ios: height <= 667 || height == 736 ? -20 : -45 }),
            backgroundColor: theme.colors.secondary,
          },
        }
      },
      rules: {
        screen: RaffleRulesScreen,
        navigationOptions: {
          title: 'Regulamento',
          headerBackTitle: null,
          headerTintColor: colors.black,
          headerStyle: {
            marginTop: Platform.select({ ios: height <= 667 || height == 736 ? -20 : -45 }),
            backgroundColor: theme.colors.secondary,
          },
        }
      }
    }, {
      initialRouteName: 'list',
      navigationOptions: ({ navigation }) => {
        let tabBarVisible = true
        let swipeEnabled = true

        if (navigation.state.index > 0) {
          tabBarVisible = false
          swipeEnabled = false
        }

        return {
          tabBarVisible,
          swipeEnabled,
        }
      }
    }),
    navigationOptions: {
      title: 'Sorteios'
    }
  },
  promotions: {
    screen: createTabStack(PromotionsScreen),
    navigationOptions: {
      title: 'Promoções'
    }
  },
  news: {
    screen: createTabStack(NewsScreen),
    navigationOptions: {
      title: 'Notícias'
    }
  },
  events: {
    screen: createTabStack(EventsScreen),
    navigationOptions: {
      title: 'Eventos'
    }
  },
}, {
  lazy: true,
  initialRouteName: 'reviews-summary',
  tabBarOptions: {
    scrollEnabled: true,
    animationEnabled: true,
    activeTintColor: theme.colors.primary,
    inactiveTintColor: theme.colors.primary,
    style: {
      paddingVertical: 15,
      backgroundColor: theme.colors.primary
    },
    tabStyle: {
      padding: 0,
      width: Platform.select({ ios: 100, android: 92 })
    },
    labelStyle: {
      margin: 0,
      padding: 2,
      fontSize: 14,
      color: 'white'
    },
    indicatorStyle: {
      backgroundColor: theme.colors.secondary,
      height: 3
    }
  }
})
