import { Dimensions } from 'react-native'
import { researchApi } from '~/api'
import { Store } from '~/store'
import { withNavigation } from 'react-navigation'
import Html from 'react-native-render-html'
import ListEmpty from '~/components/ListEmpty'
import Moment from '~/components/Moment'
import ProgressBar from '~/components/ProgressBar'
import React, { useEffect, useContext, useState } from 'react'
import SponsorList from '~/components/Media/Sponsor'
import styled from 'styled-components/native'

const { width } = Dimensions.get('screen')

const Container = styled.FlatList`
  background-color: ${props => props.theme.colors.background};
`

const Card = styled.View`
  background-color: ${props => props.theme.colors.surface};
  border-radius: 2;
  elevation: 3;
  margin-horizontal: 5;
  margin-top: 5;
  shadow-color: ${props => props.theme.colors.text};
  shadow-offset: 0px 1px;
  shadow-opacity: 0.2;
  shadow-radius: 5;
`

const Banner = styled.Image`
  height: 100;
  width: 100%;
`

const LogoContainer = styled.View`
  background-color: ${props => props.theme.colors.surface};
  border-radius: 50;
  height: 100;
  left: ${width / 2 - 50};
  position: absolute;
  top: 35;
  width: 100;
`

const Logo = styled.Image`
  align-self: center;
  border-radius: 50;
  border-color: ${props => props.theme.colors.surface};
  border-width: 4;
  height: 100;
  width: 100;
`

const Content = styled.View`
  padding-vertical: 10;
  padding-horizontal: 10;
`

const Button = styled.TouchableOpacity`
  align-items: center;
  align-self: center;
  background-color: ${props => props.theme.colors.secondary};
  border-radius: 20;
  elevation: 1;
  justify-content: center;
  padding-horizontal: 5;
  padding-vertical: 5;
  margin-vertical: 5;
  margin-horizontal: 5;
  shadow-color: ${props => props.theme.colors.text};
  shadow-offset: 0px 3px;
  shadow-opacity: 0.25;
  shadow-radius: 5;
`

const ButtonLabel = styled.Text`
  font-weight: bold;
`

const Text = styled.Text``
const View = styled.View``

export default withNavigation(({ navigation }) => {
  const { state } = useContext(Store)
  const [research, setResearch] = useState([])
  const [totalReviews, setTotalReviews] = useState(0)
  const [totalParticipants, setTotalParticipants] = useState(0)
  const [ratioCompleted, setRatioCompleted] = useState(0)
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [isListEmpty, setIsListEmpty] = useState(false)

  useEffect(() => {
    navigation.addListener('willFocus', () => {
      fetchProgress()
    })

    fetchResearch()
    fetchProgress()
  }, [])

  const fetchResearch = () => {
    setIsRefreshing(true)

    researchApi.fetchOpenedByUser({ user_id: state.user.id }).then(({ data }) => {
      if (data.research) {
        setResearch([data.research])
      } else {
        setIsListEmpty(true)
      }
      setIsRefreshing(false)
    })
  }

  const fetchProgress = () => {
    researchApi.fetchProgressByUser({ user_id: state.user.id }).then(({ data }) => {
      if (data.progress) {
        setTotalReviews(data.progress.totalReviews)
        setTotalParticipants(data.progress.totalParticipants)
        setRatioCompleted(data.progress.ratioCompleted)
      }
    })
  }

  const renderListEmpty = () => {
    return isListEmpty && !isRefreshing && (
      <ListEmpty message="NO MOMENTO NÃO HÁ AVALIAÇÕES A SEREM REALIZADAS" />
    )
  }

  return (
    <Container
      data={research}
      keyExtractor={(item, index) => index.toString()}
      refreshing={isRefreshing}
      onRefresh={fetchResearch}
      ListEmptyComponent={renderListEmpty}
      renderItem={({ item }) =>
        <>
          <Card>
            <Banner source={{ uri: item.banner }} resizeMode="cover" />
            <LogoContainer>
              <Logo source={{ uri: item.logo }} resizeMode="cover" />
            </LogoContainer>

            <View style={{ marginTop: 40, alignItems: 'center' }}>
              <Text style={{ fontWeight: 'bold' }}>{item.name}</Text>
            </View>

            <Content>
              <Html html={item.description ? item.description.body : ''} />

              <Button
                style={{ width: 120, height: 32, marginTop: 15 }}
                activeOpacity={0.7}
                onPress={() => navigation.navigate('rules', { research: item })}
              >
                <ButtonLabel>Regulamento</ButtonLabel>
              </Button>
            </Content>
          </Card>

          <Card>
            <Content>
              <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>
                Período da Avaliação: <Moment format="DD/MM/YYYY">{item.startAt}</Moment> - <Moment format="DD/MM/YYYY">{item.endAt}</Moment>
              </Text>

              <View style={{ marginTop: 5, marginBottom: 10 }}>
                <Text style={{ alignSelf: 'center' }}>Você avaliou {totalReviews} empresas de {totalParticipants}</Text>
              </View>

              <ProgressBar
                size={10}
                progress={ratioCompleted}
              />

              { totalReviews === 0 ? (
                <Button
                  style={{ width: 120, height: 32, marginTop: 15 }}
                  activeOpacity={0.7}
                  onPress={() => navigation.navigate('reviews')}
                >
                  <ButtonLabel>Participar</ButtonLabel>
                </Button>
              ) : totalReviews > 0 && (
                <Button
                  style={{ width: 180, height: 32, marginTop: 15 }}
                  activeOpacity={0.7}
                  onPress={() => navigation.navigate('reviews')}
                >
                  <ButtonLabel>Continuar Participando</ButtonLabel>
                </Button>
              )}
              { ratioCompleted === 1 &&
                <View style={{ padding: 10, backgroundColor: '#27ae60', marginTop: 20 }}>
                  <Text style={{ color: '#fff', fontWeight: 'bold' }}>Parabéns! Você avaliou todas as empresas, sua contribuição é muito importante para nós.</Text>
                </View>
              }
            </Content>
          </Card>

          { item.sponsors.length > 0 && (
            <Card>
              <Content>
                <SponsorList sponsors={ item.sponsors }/>
              </Content>
            </Card>
          )}
        </>
      }
    >
    </Container>
  )
})
