import { FlatList } from 'react-native'
import { Store, setAds } from '~/store'
import { withNavigation } from  'react-navigation'
import AdList from '~/components/AdList'
import Api from '~/api'
import ListEmpty from '~/components/ListEmpty'
import LoadIndicator from '~/components/LoadIndicator'
import Page from '~/components/Layout/Page'
import Preview from '~/components/Media/Event/Preview'
import React, { useState, useEffect, useContext } from 'react'
import Row from '~/components/Layout/Row'

export default withNavigation(({ navigation }) => {
  const [events, setEvents] = useState([])
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [isLastPage, setIsLastPage] = useState(false)
  const [page, setPage] = useState(1)
  const { state } = useContext(Store)

  useEffect(() => {
    handleFetchItems()
  }, [])

  const handleFetchItems = () => {
    Api.findAll('event', {
      sort: '-id',
      filter: { institute_id: state.institute.id }
    })
    .then(({ data }) => {
      if (data.length) {
        setEvents(data)
      } else {
        setIsLastPage(true)
      }
    })

    defineAdsEvents()
    setPage(1)
  }

  const handleLoadMore = () => {
    if (isLastPage && isRefreshing) return null

    Api.findAll('event', {
      sort: '-id',
      page: { number: page + 1 },
      filter: { institute_id: state.institute.id }
    })
    .then(({ data }) => {
      if (data.length) {
        setEvents([...events, ...data])
        setPage(page + 1)
        setIsLastPage(false)
      } else {
        setIsLastPage(true)
      }
    })
  }

  const defineAdsEvents = () => {
    Api.findAll('ad', {
      filter: {
        institute_id: state.institute.id,
        local: 'events',
        non_expired: true
      }
    })
    .then(({ data }) => {
      state.ads.events = state.ads.events.concat(data)

      setAds({ state, dispatch }, { ads: state.ads })
    })
  }

  const renderFooter = () => {
    if (isLastPage) return null
    return <LoadIndicator />
  }

  const renderListEmpty = () => {
    return isLastPage && (
      <ListEmpty message="NO MOMENTO NÃO HÁ EVENTO DISPONÍVEL" />
    )
  }

  return (
    <Page>
      <Row>
        <FlatList
          data={events}
          keyExtractor={item => item.id}
          onRefresh={handleFetchItems}
          refreshing={isRefreshing}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={1}
          ListFooterComponent={renderFooter}
          ListEmptyComponent={renderListEmpty}
          renderItem={({ item, index }) => {
            return (
              <>
                <AdList items={state.ads.events} index={index} />
                <Preview
                  key={item.id}
                  event={item}
                  onPress={() => navigation.navigate('event', { event: item })}
                />
              </>
              )
          }}
        />
      </Row>
    </Page>
  )
})
