import React from 'react'
import { StyleSheet } from 'react-native'
import { withNavigation } from 'react-navigation'
import styled from 'styled-components/native'

export default withNavigation(({ navigation }) => {
  const options = navigation.getParam('options')
  const onSelect = navigation.getParam('onSelect')

  const goBack = (value) => {
    navigation.goBack()
    onSelect(value)
  }

  const ListView = styled.FlatList``
  const OptionButton = styled.TouchableOpacity``

  const OptionButtonLabel = styled.Text`
    font-size:18;
    padding-horizontal: 20;
    padding-vertical: 10;
    border-bottom-width: ${StyleSheet.hairlineWidth};
    border-bottom-color: #D9D9D9;
  `

  return (
    <ListView
      data={options}
      keyExtractor={(item, index) => index.toString()}
      renderItem={({ item }) =>
        <OptionButton
          activeOpacity={0.7}
          onPress={() => goBack(item)}
        >
          <OptionButtonLabel>{item.name}</OptionButtonLabel>
        </OptionButton>
      }
    />
  )
})
