import { ScrollView } from 'react-native'
import { withNavigation } from 'react-navigation'
import Html from 'react-native-render-html'
import Page from '~/components/Layout/Page'
import React from 'react'
import Row from '~/components/Layout/Row'
import styled from 'styled-components/native'

const Banner = styled.Image`
  width: 100%;
  height: 150;
`

export default withNavigation(({ navigation }) => {
  const research = navigation.getParam('research')

  return (
    <Page style={ { backgroundColor: '#fff', marginBottom: 20 }}>
      <ScrollView>
        <Banner source={{ uri: research.banner }} resizeMode="cover" />
        <Row>
          <Html html={research.regulation.body} />
        </Row>
      </ScrollView>
    </Page>
  )
})
