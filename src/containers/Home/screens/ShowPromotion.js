import { Store, setPromotions, setMyPromotions } from '~/store'
import Api from '~/api'
import Promotion from '~/components/Media/Promotion'
import React, { useContext } from 'react'

const ShowPromotion = ({ navigation }) => {
  const promotion = navigation.getParam('promotion')
  const { state, dispatch } = useContext(Store)

  const handleParticipate = promotion => {
    Api.create('coupon', {
      user: { id: state.user.id },
      promotion: { id: promotion.id },
    }).then(async ({ data }) => {
      await Api.find('promotion', data.promotionId).then(({ data })=> promotion.hasCoupons = data.hasCoupons)
      promotion.coupon = data
      setMyPromotions({ state, dispatch }, { promotion })
      setPromotions({ state, dispatch }, { promotions: state.promotions })
    }).catch(error => {
      alert(error.data.detail)
    })
  }

  return (
    <Promotion promotion={promotion} coupon={promotion.coupon} onParticipatePress={handleParticipate} />
  )
}

export default ShowPromotion
