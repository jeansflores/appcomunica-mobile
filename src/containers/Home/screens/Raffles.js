import { FlatList } from 'react-native'
import { Store, setRaffles, setAds } from '~/store'
import { withNavigation } from 'react-navigation'
import AdList from '~/components/AdList'
import Api, { raffleApi } from '~/api'
import ListEmpty from '~/components/ListEmpty'
import LoadIndicator from '~/components/LoadIndicator'
import Page from '~/components/Layout/Page'
import Preview from '~/components/Media/Raffle/Preview'
import React, { useState, useEffect, useContext } from 'react'
import Row from '~/components/Layout/Row'

export default withNavigation(({ navigation }) => {
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [isLastPage, setIsLastPage] = useState(false)
  const [page, setPage] = useState(1)
  const { state, dispatch } = useContext(Store)

  useEffect(() => {
    handleFetchItems()
  }, [])

  const handleFetchItems = () => {
    Api.findAll('raffle', {
      sort: '-id',
      filter: { institute_id: state.institute.id },
      include: 'tickets'
    })
    .then(({ data }) => {
      if (data.length) {
        data.map(defineTicket)
        setRaffles({ state, dispatch }, { raffles: data })
      } else {
        setIsLastPage(true)
      }
    })

    defineAdsRaffles()
    setPage(1)
  }

  const handleLoadMore = () => {
    if (isLastPage && isRefreshing) return null

    Api.findAll('raffle', {
      sort: '-id',
      page: { number: page + 1 },
      filter: { institute_id: state.institute.id },
      include: 'tickets'
    })
    .then(({ data }) => {
      if (data.length) {
        data.map(defineTicket)
        setRaffles({ state, dispatch }, { raffles: state.raffles.concat(data) })
        setPage(page + 1)
        setIsLastPage(false)
      } else {
        setIsLastPage(true)
      }
    })
  }

  const defineAdsRaffles = () => {
    Api.findAll('ad', {
      filter: {
        institute_id: state.institute.id,
        local: 'raffles',
        non_expired: true
      }
    })
    .then(({ data }) => {
      state.ads.raffles = state.ads.raffles.concat(data)

      setAds({ state, dispatch }, { ads: state.ads })
    })
  }

  const defineTicket = (raffle) => {
    const selected = raffle.tickets.filter((ticket) => {
      return state.user.id == ticket.userId && raffle.id == ticket.raffleId
    })[0]

    raffle.ticket = typeof selected === 'undefined' ? null : selected
    delete raffle.tickets

    raffleApi.remainingVotesByUser({ user_id: state.user.id, raffle_id: raffle.id })
      .then(({ data }) => {
        raffle.remainingVotes = data
      })

    return raffle
  }

  const renderFooter = () => {
    if (isLastPage) return null
    return <LoadIndicator />
  }

  const renderListEmpty = () => {
    return isLastPage && (
      <ListEmpty message="NO MOMENTO NÃO HÁ SORTEIO DISPONÍVEL" />
    )
  }

  return (
    <Page>
      <Row>
        <FlatList
          data={state.raffles}
          keyExtractor={item => item.id}
          onRefresh={handleFetchItems}
          refreshing={isRefreshing}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={1}
          ListFooterComponent={renderFooter}
          ListEmptyComponent={renderListEmpty}
          renderItem={({ item, index }) => {
            return (
              <>
                <AdList items={state.ads.raffles} index={index} />
                <Preview
                  key={item.id}
                  raffle={item}
                  onPress={
                    () => navigation.navigate('raffle', { raffle: item })
                  }
                />
              </>
            )
          }}
        />
      </Row>
    </Page>
  )
})
