import React from 'react'
import Event from '~/components/Media/Event'

const ShowEvent = ({ navigation }) => {
  const event = navigation.getParam('event')

  return <Event event={event} />
}

export default ShowEvent
