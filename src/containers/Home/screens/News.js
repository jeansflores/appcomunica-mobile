import { FlatList } from 'react-native'
import { Store, setAds } from '~/store'
import { withNavigation } from  'react-navigation'
import AdList from '~/components/AdList'
import Api from '~/api'
import ListEmpty from '~/components/ListEmpty'
import LoadIndicator from '~/components/LoadIndicator'
import Page from '~/components/Layout/Page'
import Preview from '~/components/Media/Post/Preview'
import React, { useState, useEffect, useContext } from 'react'
import Row from '~/components/Layout/Row'

export default withNavigation(({ navigation }) => {
  const [posts, setPosts] = useState([])
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [isLastPage, setIsLastPage] = useState(false)
  const [page, setPage] = useState(1)
  const { state } = useContext(Store)

  useEffect(() => {
    handleFetchItems()
  }, [])

  const handleFetchItems = () => {
    Api.findAll('post', {
      sort: '-id',
      filter: { institute_id: state.institute.id }
    })
    .then(({ data }) => {
      if (data.length) {
        setPosts(data)
      } else {
        setIsLastPage(true)
      }
    })

    defineAdsNews()
    setPage(1)
  }

  const handleLoadMore = () => {
    if (isLastPage && isRefreshing) return null

    Api.findAll('post', {
      sort: '-id',
      page: { number: page + 1 },
      filter: { institute_id: state.institute.id }
    })
    .then(({ data }) => {
      if (data.length) {
        setPosts([...posts, ...data])
        setPage(page + 1)
        setIsLastPage(false)
      } else {
        setIsLastPage(true)
      }
    })
  }

  const defineAdsNews = () => {
    Api.findAll('ad', {
      filter: {
        institute_id: state.institute.id,
        local: 'news',
        non_expired: true
      }
    })
    .then(({ data }) => {
      state.ads.news = state.ads.news.concat(data)

      setAds({ state, dispatch }, { ads: state.ads })
    })
  }

  const renderFooter = () => {
    if(isLastPage) return null
    return <LoadIndicator />
  }

  const renderListEmpty = () => {
    return isLastPage && (
      <ListEmpty message="NO MOMENTO NÃO HÁ NOTÍCIA DISPONÍVEL" />
    )
  }

  return (
    <Page>
      <Row>
        <FlatList
          data={posts}
          keyExtractor={item => item.id}
          onRefresh={handleFetchItems}
          refreshing={isRefreshing}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={1}
          ListFooterComponent={renderFooter}
          ListEmptyComponent={renderListEmpty}
          renderItem={({ item, index }) =>
          <>
            <AdList items={state.ads.news} index={index} />
            <Preview
              key={item.id}
              post={item}
              onPress={() => navigation.navigate('post', { post: item })}
            />
          </>
          }
        />
      </Row>
    </Page>
  )
})
