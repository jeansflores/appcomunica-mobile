import Raffle from '~/components/Media/Raffle'
import React, { useContext } from 'react'
import { Store } from '~/store'

const ShowRaffle = ({ navigation }) => {
  const { state, dispatch } = useContext(Store)
  const raffle = state.raffles.find(raffle => raffle.id == navigation.getParam('raffle').id)

  return (
    <Raffle raffle={raffle} ticket={raffle.ticket} navigation={navigation} />
  )
}

export default ShowRaffle
