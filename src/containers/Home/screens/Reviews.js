import { Store } from '~/store'
import { withNavigation } from 'react-navigation'
import Api, { researchApi } from '~/api'
import ListEmpty from '~/components/ListEmpty'
import LoadIndicator from '~/components/LoadIndicator'
import Page from '~/components/Layout/Page'
import React, { useState, useEffect, useContext } from 'react'
import Card from '~/components/Media/Review/Card'
import styled from 'styled-components/native'
import { FlatList, View } from 'react-native'
import color from 'color'
import { sliceText } from '~/utils'
import Toast from 'react-native-root-toast'
import Icon from 'react-native-vector-icons/FontAwesome5'

export default withNavigation(({ navigation }) => {
  const search = navigation.getParam('search')
  const [items, setItems] = useState([])
  const [offset, setOffset] = useState(0)
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [isLastPage, setIsLastPage] = useState(false)
  const [selectedReviewedId, setSelectedReviewedId] = useState(false)
  const [selectedCategoryId, setSelectedCategoryId] = useState(null)
  const [selectedReviewed, setSelectedReviewed] = useState({ name: 'Não avaliadas', value: false })
  const [selectedCategory, setSelectedCategory] = useState({})
  const [optionsCategories, setOptionsCategories] = useState([])
  const { state } = useContext(Store)

  const optionsRevieweds = [
    { name: 'Todas', value: null },
    { name: 'Avaliadas', value: true },
    { name: 'Não avaliadas', value: false }
  ]

  useEffect(() => {
    handleFetchItems()
  }, [selectedReviewed, selectedCategory, search])

  const handleFetchItems = () => {
    setIsRefreshing(true)

    researchApi.fetchOpenedByUser({
      ...(typeof(search) === 'string' && { trade_name: search }),
      ...(selectedReviewedId !== null && { reviewd: selectedReviewedId }),
      ...(selectedCategoryId !== null && { category_id: selectedCategoryId }),
      user_id: state.user.id,
    }).then(({ data }) => {
      if (data.research.participants.length) {
        setItems(data.research.participants)
        setOptionsCategories(data.research.categories)
        setIsLastPage(false)
      } else {
        setItems([])
        setIsLastPage(true)
      }
    })

    setIsRefreshing(false)
    setOffset(0)
  }

  const handleLoadMore = () => {
    if (isLastPage) {
      return
    }

    researchApi.fetchOpenedByUser({
      ...(typeof(search) === 'string' && { trade_name: search }),
      ...(selectedReviewedId !== null && { reviewd: selectedReviewedId }),
      ...(selectedCategoryId !== null && { category_id: selectedCategoryId }),
      user_id: state.user.id,
      page: { offset: items.length },
    }).then(({ data }) => {
      if (data.research.participants.length) {
        setItems([...items, ...data.research.participants])
        setOffset(offset + data.research.participants.length)
        setIsLastPage(false)
      } else {
        setIsLastPage(true)
      }
    })
  }

  const handleRating = (rating, participant, vote) => {
    if (vote && vote.rating === rating) {
      return
    }

    if (vote) {
      Api.update('vote', {
        id: vote.id,
        rating
      }).then(() => {
        vote.rating = rating

        Toast.show(`Obrigado(a) por reavaliar a empresa ${participant.company.tradeName}.`, {
          duration: Toast.durations.LONG,
          position: Toast.positions.BOTTOM,
          shadow: true,
          animation: true,
          hideOnPress: true,
          backgroundColor: 'green',
          delay: 0,
        })
      })
    } else {
      Api.create('vote', {
        rating,
        user: { id: state.user.id },
        researchParticipant: { id: participant.id },
      }).then(({ data }) => {
        participant.vote = data

        Toast.show(`Obrigado(a) por avaliar a empresa ${participant.company.tradeName}.`, {
          duration: Toast.durations.LONG,
          position: Toast.positions.BOTTOM,
          shadow: true,
          animation: true,
          hideOnPress: true,
          backgroundColor: 'green',
          delay: 0,
        })
      })

      if (selectedReviewedId != null) {
        setTimeout(() => {
          setItems(items.filter(item => item.id !== participant.id))
        }, 500);
      }
    }
  }

  const onSelectReviewed = (option) => {
    setSelectedReviewed(option)
    setSelectedReviewedId(option.value)
  }

  const onSelectCategory = (option) => {
    setSelectedCategory(option)
    setSelectedCategoryId(option.value)
  }

  const renderFooter = () => {
    if(isLastPage) return null

    return <LoadIndicator />
  }

  const renderListEmpty = () => {
    return isLastPage && (
      <ListEmpty message="NENHUMA EMPRESA ENCONTRADA" />
    )
  }

  const Filter = styled.View`
    background-color: ${props => props.theme.colors.surface};
    elevation: 3;
    flex-direction: row;
    flex: 0.1;
    height: 45;
    max-height: 45;
    min-height: 45;
    shadow-color: ${props => props.theme.colors.text};
    shadow-opacity: 0.22;
    shadow-radius: 2;
    width: 100%;
  `

  const ButtonFilter = styled.TouchableOpacity`
    flex: 0.9;
    justify-content: center;
    padding-bottom: 10;
    padding-left: 10;
    padding-right: 10;
    padding-top: 10;
  `

  const ButtonFilterLabel = styled.Text`
    align-self: center;
    font-size: 18;
  `

  const DividerVertical = styled.View`
    border-left-color: ${props => color(props.theme.colors.text).alpha(0.12).rgb().string()};
    border-left-width: 1;
  `

  return (
    <Page>
      <View style={{flex: 1}}>
        <Filter>
          <ButtonFilter
            activeOpacity={0.7}
            onPress={() => {
              navigation.navigate('filter', { options: optionsRevieweds, onSelect: onSelectReviewed }
            )}}
          >
            <ButtonFilterLabel
              ellipsizeMode='tail'
              numberOfLines={1}
            >
              <Icon name="filter" size={15} />{` ${selectedReviewed.name}`}
            </ButtonFilterLabel>
          </ButtonFilter>
          <DividerVertical />
          <ButtonFilter
            activeOpacity={0.7}
            onPress={() => {
              navigation.navigate('filter', { options: optionsCategories, onSelect: onSelectCategory }
            )}}
          >
            { selectedCategory.name ? (
              <ButtonFilterLabel
                ellipsizeMode='tail'
                numberOfLines={1}
              >
                <Icon name="filter" size={15} />{` ${selectedCategory.name}`}
              </ButtonFilterLabel>
            ) : (
              <ButtonFilterLabel
                ellipsizeMode='tail'
                numberOfLines={1}
              >
                <Icon name="filter" size={15} />{` Categoria`}
              </ButtonFilterLabel>
            )}
          </ButtonFilter>
        </Filter>
        <FlatList
          style={{flex: 0.9, marginHorizontal: 5, marginVertical: 5}}
          data={items}
          keyExtractor={(item, index) => index.toString()}
          refreshing={isRefreshing}
          onRefresh={handleFetchItems}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={1}
          ListFooterComponent={renderFooter}
          ListEmptyComponent={renderListEmpty}
          renderItem={({ item }) =>
            <Card
              key={item.company.id}
              title={item.company.tradeName}
              address={item.company.address}
              image={item.company.logoUrl}
              rating={item.vote ? item.vote.rating : 0}
              onFinishRating={rating => handleRating(rating, item, item.vote)}
            />
          }
        />
      </View>
    </Page>
  )
})
