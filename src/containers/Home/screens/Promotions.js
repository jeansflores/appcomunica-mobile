import { FlatList } from 'react-native'
import { Store, setPromotions, setAds } from '~/store'
import { withNavigation } from 'react-navigation'
import AdList from '~/components/AdList'
import Api, { promotionApi } from '~/api'
import ListEmpty from '~/components/ListEmpty'
import LoadIndicator from '~/components/LoadIndicator'
import Page from '~/components/Layout/Page'
import Preview from '~/components/Media/Promotion/Preview'
import React, { useState, useEffect, useContext } from 'react'
import Row from '~/components/Layout/Row'

export default withNavigation(({ navigation }) => {
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [isLastPage, setIsLastPage] = useState(false)
  const [page, setPage] = useState(1)
  const { state, dispatch } = useContext(Store)

  useEffect(() => {
    handleFetchItems()
  }, [])

  const handleFetchItems = () => {
    Api.findAll('promotion', {
      sort: '-id',
      filter: { institute_id: state.institute.id },
      include: 'company,coupons'
    })
    .then(({ data }) => {
      if (data.length) {
        data.map(defineCoupon)
        setPromotions({ state, dispatch }, { promotions: data })
      } else {
        setIsLastPage(true)
      }
    })

    defineAdsPromotions()
    setPage(1)
  }

  const handleLoadMore = () => {
    if (isLastPage && isRefreshing) return null

    Api.findAll('promotion', {
      sort: '-id',
      page: { number: page + 1 },
      filter: { institute_id: state.institute.id },
      include: 'company,coupons'
    })
    .then(({ data }) => {
      if (data.length) {
        data.map(defineCoupon)
        setPromotions({ state, dispatch }, { promotions: state.promotions.concat(data) })
        setPage(page + 1)
        setIsLastPage(false)
      } else {
        setIsLastPage(true)
      }
    })
  }

  const defineAdsPromotions = () => {
    Api.findAll('ad', {
      filter: {
        institute_id: state.institute.id,
        local: 'promotions',
        non_expired: true
      }
    })
    .then(({ data }) => {
      state.ads.promotions = state.ads.promotions.concat(data)

      setAds({ state, dispatch }, { ads: state.ads })
    })
  }

  const defineCoupon = (promotion) => {
    const selected = promotion.coupons.filter((coupon) => {
      return state.user.id == coupon.userId && promotion.id == coupon.promotionId
    })[0]

    promotion.coupon = typeof selected === 'undefined' ? null : selected
    delete promotion.coupons

    promotionApi.remainingVotesByUser({ user_id: state.user.id, promotion_id: promotion.id })
      .then(({ data }) => {
        promotion.remainingVotes = data
      })

    return promotion
  }

  const renderFooter = () => {
    if (isLastPage) return null

    return <LoadIndicator />
  }

  const renderListEmpty = () => {
    return isLastPage && (
      <ListEmpty message="NO MOMENTO NÃO HÁ PROMOÇÃO DISPONÍVEL" />
    )
  }

  return (
    <Page>
      <Row>
        <FlatList
          data={state.promotions}
          keyExtractor={item => item.id}
          onRefresh={handleFetchItems}
          refreshing={isRefreshing}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={1}
          ListFooterComponent={renderFooter}
          ListEmptyComponent={renderListEmpty}
          renderItem={({ item, index }) => {
            return (
              <>
                <AdList items={state.ads.promotions} index={index} />
                <Preview
                  key={item.id}
                  promotion={item}
                  onPress={
                    () => navigation.navigate('promotion', { promotion: item })
                  }
                />
              </>
            )
          }}
        />
      </Row>
    </Page>
  )
})
