import MyPromotions from '~/components/Media/MyPromotions'
import React from 'react'

const ShowMyPromotion = ({ navigation }) => {
  const promotion = navigation.getParam('promotion')

  return (
    <MyPromotions promotion={promotion} coupon={promotion.coupon} />
  )
}

export default ShowMyPromotion
