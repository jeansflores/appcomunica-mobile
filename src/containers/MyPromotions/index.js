import { createStackNavigator } from 'react-navigation'
import { theme, colors } from '~/utils'
import MyPromotions from './screens/MyPromotions'
import MyPromotionScreen from './screens/ShowMyPromotions';

export default createStackNavigator({
  'my-promotions': {
    navigationOptions: {
      header: null,
      headerBackTitle: null
    },
    screen: createStackNavigator({
      list: {
        screen: MyPromotions,
        navigationOptions: {
          headerTintColor: colors.white,
          headerStyle: {
            backgroundColor: theme.colors.primary,
          },
          title: `Minhas Promoções`
        },
      },
      'view-promotion': {
        screen: MyPromotionScreen,
        navigationOptions: {
          title: 'Promoção',
          headerStyle: {
            backgroundColor: theme.colors.secondary,
          },
        },
      }
    }),
  },
})
