import { Platform } from 'react-native'

export const stringLimit = (value, limit = 100, end = '...') => {
  if (!value) {
    return ''
  }

  if (value.length <= limit) {
    return value
  }

  return value.substring(limit) + end
}

export const sliceText = (text, size) => {
  if (text.length > size) {
    return text.slice(0, size).concat('...')
  }

  return text
}

export const timeAgo = unixTime => {
  const now = Date.now() / 1000
  const time = unixTime / 1000
  const between = Math.abs(now - time)

  if (between < 60) {
    return ~~between + 's'
  } else if (between < 3600) {
    return ~~(between / 60) + 'm'
  } else if (between < 86400) {
    return ~~(between / 3600) + 'h'
  } else if (between < 2592000) {
    return ~~(between / 86400) + 'd'
  } else if (between < 31536000) {
    return ~~(between / 2592000) + 'M'
  } else {
    return ~~(between / 31536000) + 'A'
  }
}

export const colors = Object.freeze({
  black: '#000',
  white: '#fff',
  facebook: '#3b5998',
  twitter: '#55acee',
  google: '#dd4b39',
})

export const fonts = Platform.select({
  ios: {
    regular: 'Helvetica Neue',
    medium: 'HelveticaNeue-Medium',
    light: 'HelveticaNeue-Light',
    thin: 'HelveticaNeue-Thin',
  },
  android: {
    regular: 'sans-serif',
    medium: 'sans-serif-medium',
    light: 'sans-serif-light',
    thin: 'sans-serif-thin',
  },
})

export const theme = Object.freeze({
  dark: false,
  colors: {
    primary: '#143a85',
    secondary: '#ffcc29',
    accent: '#0172bc',
    background: '#e8e8e8',
    surface: colors.white,
    text: colors.black,
    placeholder: colors.black,
    grey: 'grey',
  },
  fonts
})
