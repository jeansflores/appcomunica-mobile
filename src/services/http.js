import axios from 'axios'
import { Alert } from 'react-native'
import { getToken, removeToken } from '~/services/auth'

const http = axios.create({
  baseURL: 'https://api.appcomunica.com.br/api/v1',
  timeout: 5000,
  headers: {
    'Accept': 'application/vnd.api+json',
    'Content-Type': 'application/json',
    'Institute-Id': '1',
  },
})

http.interceptors.request.use(
  async config => {
    const token = await getToken()

    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }

    return config
  },
  error => {
    return Promise.reject(error)
  }
)

http.interceptors.response.use(
  response => response,
  async error => {
    if (error.response === undefined) {
      Alert.alert('Não foi possível se conectar ao servidor')
    }

    if (error.response.status === 401) {
      await removeToken()

      // TODO: Navigate to login screen
      // https://reactnavigation.org/docs/en/navigating-without-navigation-prop.html
    }


    return Promise.reject(error)
  }
)

export default http
