import { authApi } from '~/api'
import { AsyncStorage } from 'react-native'

export const fetchToken = credentials => {
  return authApi.token(credentials)
}

export const getToken = () => {
  return AsyncStorage.getItem('token')
}

export const setToken = token => {
  return AsyncStorage.setItem('token', token)
}

export const removeToken = () => {
  return AsyncStorage.removeItem('token')
}

export const fetchUser = () => {
  return authApi.me()
}

export const logout = () => {
  return removeToken()
}

export const loginWithCredentials = credentials => {
  return fetchToken(credentials)
    .then(async ({ data }) => {
      await setToken(data.token)

      return Promise.resolve(data.token)
    }).catch(error => {
      return Promise.reject(error)
    })
}

export const register = payload => {
  return authApi.register(payload)
}

export const resetToken = email => {
  return authApi.resetToken(email)
}

export const resetPassword = payload => {
  return authApi.resetPassword(payload)
}
