import color from 'color'
import React from 'react'
import PropTypes from 'prop-types'
import styled, { withTheme } from 'styled-components/native'

const Container = styled.View`
  position: relative;
  height: ${props => props.size};
  width: 100%;
  background-color: ${props => color(props.color).lighten(0.5).rgb().string()};
`

const Progress = styled.View`
  position: absolute;
  height: ${props => props.size};
  width: ${props => props.value * 100}%;
  background-color: ${props => props.color};
`

const ProgressBar = props => {
  const { theme, progress, ...rest } = props

  rest.color = rest.color || theme.colors.secondary

  return (
    <Container {...rest}>
      <Progress {...rest} value={progress} />
    </Container>
  )
}

ProgressBar.propTypes = {
  progress: PropTypes.number,
  size: PropTypes.number,
  color: PropTypes.string,
}

ProgressBar.defaultProps = {
  progress: 0,
  size: 5,
}

export default withTheme(ProgressBar)
