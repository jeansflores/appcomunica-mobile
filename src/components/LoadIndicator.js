import React from 'react'
import styled from 'styled-components/native'
import { ActivityIndicator } from 'react-native'
import { theme } from '~/utils'

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  margin-top: 40;
  margin-bottom: 40;
`

const LoadIndicator = ({ color }) => (
  <Container>
    <ActivityIndicator size="large" color={color || theme.colors.primary}/>
  </Container>
)

export default LoadIndicator
