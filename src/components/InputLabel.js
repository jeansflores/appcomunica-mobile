import React, { useState } from 'react'
import styled from 'styled-components/native'
import { theme } from '~/utils'

const GroupInput = styled.View`
  margin-vertical: 5;
  margin-horizontal: 5;
`

const Label = styled.Text``

const Input = styled.TextInput`
  border-bottom-width: 1;
`

const InputLabel = ({ label, colorFocus, ...props }) => {
  const [ isFocused, setIsFocused ] = useState(false)

  const handleFocus = () => setIsFocused(true)

  const handleBlur = () => setIsFocused(false)

  return (
    <GroupInput>
      <Label>{label}</Label>
      <Input
        onFocus={handleFocus}
        onBlur={handleBlur}
        style={{ borderBottomColor: isFocused ? colorFocus || theme.colors.primary : 'grey' }}
        {...props}
      />
    </GroupInput>
  )
}

export default InputLabel;
