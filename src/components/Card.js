import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'

const Container = styled.TouchableOpacity`
  background-color: ${props => props.theme.colors.surface};
  border-radius: 2;
  elevation: 3;
  flex-direction: row;
  margin-bottom: 5;
  padding-horizontal: 5;
  padding-vertical: 5;
  shadow-color: ${props => props.theme.colors.text};
  shadow-offset: 0px 1px;
  shadow-opacity: 0.2;
  shadow-radius: 5;
`

const Header = styled.View`
  flex-direction: row;
  flex: 0.4;
  max-height: 105;
  padding-right: 10;
`

const Image = styled.Image`
  flex: 1;
  height: 105;
`

const Content = styled.View`
  flex-direction: row;
  flex: 0.6;
`

const LeftBar = styled.View`
  flex-direction: column;
  flex: 1;
  height: 100;
  justify-content: center;
`

const Title = styled.Text`
  font-size: 18;
  font-weight: bold;
`

const Subtitle = styled.Text`
  font-size: 12;
`

const Card = ({ title, subtitle, image, onPress }) => (
  <Container activeOpacity={0.75} onPress={onPress}>
    <Header>
      <Image
        source={ typeof image === 'string' ? { uri: image } : image}
        resizeMode="cover"
      />
    </Header>

    <Content>
      <LeftBar>
        <Title>{title}</Title>
        { subtitle && <Subtitle>{subtitle}</Subtitle> }
      </LeftBar>
    </Content>
  </Container>
)

Card.propTypes = {
  title: PropTypes.string.isRequired,
  image: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired,
  onPress: PropTypes.func
}

Card.defaultProps = {
  onPress: () => {}
}

export default Card
