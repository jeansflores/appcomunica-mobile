import React from 'react'
import PropTypes from 'prop-types'
import StyledText from './StyledText'
import styled from 'styled-components/native'

const Text = styled(StyledText)`
  font-size: 14;
  line-height: 20;
  margin-vertical: 2;
`

const Title = props => (
  <Text
    {...props}
    alpha={0.87}
    family="regular"
    style={props.style}
  />
)

Title.propTypes = {
  style: PropTypes.any,
}

export default Title
