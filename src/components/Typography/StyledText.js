import color from 'color'
import React from 'react'
import Text from './Text'
import PropTypes from 'prop-types'
import { withTheme } from 'styled-components/native'

const StyledText = props => {
  const { alpha, family, theme, style, ...rest } = props
  const textColor = color(theme.colors.text).alpha(alpha).rgb().string()
  const fontFamily = theme.fonts[family]

  return (
    <Text
      {...rest}
      style={[{ color: textColor, fontFamily, textAlign: 'left' }, style]}
    />
  )
}

StyledText.propTypes = {
  alpha: PropTypes.number,
  family: PropType.OneOf(['regular', 'medium', 'light', 'thin']),
  style: PropTypes.any,
}

export default withTheme(StyledText);
