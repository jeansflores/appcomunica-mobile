import React from 'react'
import PropTypes from 'prop-types'
import StyledText from './StyledText'
import styled from 'styled-components/native'

const Text = styled(StyledText)`
  font-size: 20;
  line-height: 30;
  margin-vertical: 2;
`

const Title = props => (
  <Text
    {...props}
    alpha={0.87}
    family="medium"
    style={props.style}
  />
)

Title.propTypes = {
  style: PropTypes.any,
}

export default Title
