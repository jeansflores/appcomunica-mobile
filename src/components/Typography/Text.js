import React from 'react'
import PropTypes from 'prop-types'
import { Text as NativeText } from 'react-native'
import { withTheme } from 'styled-components/native'

const Text = props => {
  const { style, theme, ...rest } = props;

  return (
    <NativeText
      {...rest}
      style={[
        {
          fontFamily: theme.fonts.regular,
          color: theme.colors.text,
          textAlign: 'left',
        },
        style
      ]}
    />
  )
}

Text.propTypes = {
  style: PropTypes.any,
}

export default withTheme(Text);
