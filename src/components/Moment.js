import 'moment/locale/pt-br'
import Moment from 'react-moment'
import { Text } from 'react-native'

Moment.globalLocale = 'pt-br'
Moment.globalLocal = true
Moment.globalElement = Text

export default Moment
