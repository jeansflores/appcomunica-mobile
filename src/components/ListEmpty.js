import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components/native'

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  margin-top: 40;
`

const Message = styled.Text`
  font-size: 15;
  font-weight: bold;
  text-align: center;
  color: ${props => props.theme.colors.primary};
`

const ListEmpty = ({ message }) => (
  <Container>
    <Message>
      {message}
    </Message>
  </Container>
)

ListEmpty.propTypes = {
  message: PropTypes.string.isRequired,
}

export default ListEmpty
