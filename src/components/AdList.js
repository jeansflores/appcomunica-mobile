import React from 'react'
import styled from 'styled-components/native'

const Container = styled.View`
  background-color: ${props => props.theme.colors.surface};
  elevation: 3;
  margin-horizontal: 15;
  margin-vertical: 15;
  shadow-color: ${props => props.theme.colors.text};
  shadow-offset: 0px 1px;
  shadow-opacity: 0.2;
  shadow-radius: 5;
`

const Image = styled.Image`
  height: 60;
`

const Label = styled.Text`
  background-color: rgba(255, 255, 255, 0.4);
  border-top-left-radius: 4;
  bottom: 0;
  font-size: 12;
  font-weight: bold;
  padding-left: 3;
  position: absolute;
  right: 0;
`

export default AdList = ({ items, index }) =>{
  const canShowAd = () => {
    return items.length > 0 && index % 5 === 0
  }

  const extractImage = () => {
    return items[Math.floor(Math.random() * items.length)].image
  }

  return canShowAd() && (
    <Container>
      <Image source={{uri: extractImage()}} resizeMode="cover"/>
      <Label>Anúncio</Label>
    </Container>
  )
}
