import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding-vertical: 10;
  padding-horizontal: 10;
  margin-bottom: 5;
  border-radius: 2;
  shadow-color: ${props => props.theme.colors.text};
  shadow-opacity: 0.22;
  shadow-radius: 2;
  elevation: 3;
  background-color: ${props => props.theme.colors.surface};
`

const Message = styled.Text`
  text-align: center;
  font-size: 15;
  font-weight: bold;
`

const CustomAlert = ({ message }) => (
  <Container>
    <Message>
      {message}
    </Message>
  </Container>
)

CustomAlert.propTypes = {
  message: PropTypes.string.isRequired,
}

export default CustomAlert
