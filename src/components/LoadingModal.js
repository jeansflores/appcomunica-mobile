import React from 'react'
import styled from 'styled-components/native'
import { ActivityIndicator } from 'react-native'
import { theme } from '~/utils'

const Container = styled.View`
  background-color: rgba(0, 0, 0, 0.6);
  flex: 1;
  height: 100%;
  justify-content: center;
  position: absolute;
  width: 100%;
`

const LoadingModal = () => (
  <Container>
    <ActivityIndicator size="large" color={theme.colors.surface}/>
  </Container>
)

export default LoadingModal
