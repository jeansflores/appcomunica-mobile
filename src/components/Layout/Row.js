import React from 'react'
import styled from 'styled-components/native'

const Container = styled.View`
  margin-top: 5;
  margin-horizontal: 5;
`

export default ({ children }) => (
  <Container>{children}</Container>
)
