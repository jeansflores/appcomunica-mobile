import React from 'react'
import { withNavigation } from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome5'
import styled, { withTheme } from 'styled-components/native'

const Container = styled.View`
  flex: 1;
`

const IconButton = styled.TouchableOpacity`
  padding-vertical: 17;
  padding-horizontal: 17;
`

const DrawerButton = ({ navigation, theme }) => (
  <Container>
    <IconButton
      activeOpacity={1}
      onPress={() => navigation.toggleDrawer()}
    >
      <Icon name='bars' size={18} style={{ color: theme.colors.surface }} />
    </IconButton>
  </Container>
)

export default withTheme(withNavigation(DrawerButton))
