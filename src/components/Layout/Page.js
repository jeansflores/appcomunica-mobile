import React from 'react'
import styled from 'styled-components/native'

const Container = styled.SafeAreaView`
  flex: 1;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: ${props => props.theme.colors.background};
`

export default props => <Container {...props} />
