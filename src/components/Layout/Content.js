import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'

const Container = styled.View`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: ${props => props.transparent ? 'transparent' : props.theme.colors.surface};
`

const Content = props => (
  <Container {...props} />
)

Content.propTypes = {
  transparent: PropTypes.bool
}

Content.defaultProps = {
  transparent: false
}

export default Content
