import { Auth } from '~/services'
import { DrawerItems } from 'react-navigation'
import { GoogleSignin } from 'react-native-google-signin'
import { LoginManager } from 'react-native-fbsdk'
import { Store } from '~/store'
import { StyleSheet, Platform } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import React, { useEffect, useContext } from 'react'
import styled from 'styled-components/native'
import OneSignal from 'react-native-onesignal'

const Container = styled.View({
  ...StyleSheet.absoluteFillObject,
})

const Content = styled.SafeAreaView`
  flex: 1;
`

const LogoutLink = styled.Text`
  bottom: 0;
  font-weight: bold;
  padding-horizontal: 10;
  padding-vertical: 10;
  position: absolute;
`

const ContainerAvatar = styled.View`
  align-items: center;
  background-color: #11357B;
  padding-horizontal: 10;
  padding-vertical: 10;
`

const Avatar = styled.Image`
  border-color: white;
  border-radius: ${ Platform.OS === 'ios' ? 35 : 75 };
  border-width: 2;
  height: 70;
  margin-bottom: 10;
  width: 70;
`

const UserName = styled.Text`
  color: white;
  font-weight: bold;
  text-align: center
`

const UserEmail = styled.Text`
  color: white;
  font-size: 11;
`

const ButtonEditUser = styled.TouchableOpacity`
  padding-horizontal: 7;
  padding-vertical: 7;
  position: absolute;
  right: 0;
`

const ButtonContent = styled.View`
  background-color: #A8A8A8;
  border-radius: 30;
  border-width 1;
  padding-horizontal: 4;
  padding-vertical: 4;
`

const Drawer = props => {
  const { state, dispatch } = useContext(Store)

  useEffect(() => {
    OneSignal.init("daabec13-42c9-4d4e-98cd-34b3232a9559")
  }, [])

  const handleLogout = async () => {
    await Auth.logout()
    GoogleSignin.signOut()
    LoginManager.logOut()
    OneSignal.removeEventListener('received', () => {})
    OneSignal.removeEventListener('opened', () => {})
    OneSignal.removeEventListener('ids', () => {})

    props.navigation.navigate('login')
  }

  return (
    <Container>
      <Content forceInset={{ to: 'always', horizontal: 'never' }}>
        <ContainerAvatar>
          { state.user.avatar ? (
            <Avatar source={{ uri: state.user.avatar }} resizeMode="cover" />
          ) : (
            <Avatar source={ require('~/assets/img/default-icon-user.png') } resizeMode="cover" />
          )}
          <UserName>{state.user.name}</UserName>
          <UserEmail>{state.user.email}</UserEmail>
        </ContainerAvatar>
        <ButtonEditUser
          activeOpacity={0.7}
          onPress={() => {
            props.navigation.navigate('user-edit')
          }}
        >
          <ButtonContent>
            <Icon name='pen' size={8} color="white"></Icon>
          </ButtonContent>
        </ButtonEditUser>
        <DrawerItems {...props} />
      </Content>

      <LogoutLink onPress={() => handleLogout()}>Sair</LogoutLink>
    </Container>
  )
}

export default Drawer
