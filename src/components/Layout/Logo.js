import React from 'React'
import styled from 'styled-components/native'

const Container = styled.View`
  flex: 1;
  flex-direction: row;
  position: absolute;
  justify-content: center;
  align-items: center;
  max-height: 100%;
`

const Brand = styled.Text`
  color: ${p => p.theme.colors.surface};
  font-weight: bold;
  font-size: 17;
  text-align: center;
`

const Logo = () => {
  return (
    <Container>
      <Brand>CDL Mais Você</Brand>
    </Container>
  )
}

export default Logo
