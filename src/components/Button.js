import { theme } from '~/utils'
import PropTypes from 'prop-types'
import styled, { withTheme } from 'styled-components/native'

const NativeButton = styled.Button`
  color: ${props => theme.colors[props.color]};
`

const Button = props => (
  <NativeButton>{props.children}</NativeButton>
)

Button.PropTypes = {
  color: PropTypes.oneOf(['primary', 'secondary', 'accent']),
  icon: PropTypes.string,
  rounded: PropTypes.bool,
}

Button.defaultProps = {
  color: 'primary',
  icon: null,
  rounded: false,
}

export default withTheme(Button)
