import color from 'color'
import { StyleSheet } from 'react-native'
import styled from 'styled-components/native'

export default styled.View`
  margin-vertical: 10;
  border-bottom-width: ${StyleSheet.hairlineWidth};
  border-bottom-color: ${props => color(props.theme.colors.text).alpha(0.12).rgb().string()};
`
