import Page from './Layout/Page'
import Content from './Layout/Content'
import Row from './Layout/Row'

export default {
  Page,
  Content,
  Row,
}
