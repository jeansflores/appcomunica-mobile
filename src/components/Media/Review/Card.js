import { AirbnbRating } from 'react-native-ratings'
import { Platform } from 'react-native'
import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components/native'

const Container = styled.View`
  background-color: ${props => props.theme.colors.surface};
  border-radius: 2;
  elevation: 1;
  flex-direction: row;
  margin-bottom: 5;
  padding-horizontal: 10;
  padding-vertical: 10;
  shadow-color: ${props => props.theme.colors.text};
  shadow-offset: 0px 1px;
  shadow-opacity: 0.08;
  shadow-radius: 5;
`

const Header = styled.View`
  flex-direction: row;
  flex: 0.3;
  justify-content: center;
  padding-right: 10;
`

const Image = styled.Image`
  align-self: center;
  border-radius: ${ Platform.OS === 'ios' ? 40 : 50 };
  height: 78;
  width: 78;
`

const Content = styled.View`
  align-items: flex-start;
  flex: 0.7;
`

const Title = styled.Text`
  font-size: 15;
  font-weight: bold;
`

const Subtitle = styled.Text`
  font-size: 12;
  margin-vertical: 5;
`

const Address = styled.Text`
  font-size: 12;
  margin-vertical: 5;
`

const Card = ({ title, address, image, rating, onFinishRating }) => (
  <Container activeOpacity={0.75}>
    <Header>
      { image ? (
        <Image
          source={ typeof image === 'string' ? { uri: image } : image }
          resizeMode="cover"
        />
      ) : (
        <Image
          source={ require('~/assets/img/default-icon-building.png') }
          resizeMode="cover"
        />
      )}
    </Header>

    <Content>
      <Title>{title}</Title>
      {/* <Subtitle>Categoria: {subtitle}</Subtitle> */}
      <Address>
        End: {address.street}, {address.complement != '' && address.complement != null ? ` ${address.complement},` : ''}
        {address.number != '' && address.number != null ? ` Nº ${address.number},` : ''} {address.district}
      </Address>
      <AirbnbRating
        size={30}
        count={5}
        reviews={['Péssimo', 'Ruim', 'OK', 'Bom', 'Excelente']}
        defaultRating={rating}
        showRating={false}
        onFinishRating={onFinishRating}
      />
    </Content>
  </Container>
)

Card.propTypes = {
  title: PropTypes.string.isRequired,
  // subtitle: PropTypes.string.isRequired,
  rating: PropTypes.number,
  image: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  onFinishRating: PropTypes.func,
}

Card.defaultProps = {
  rating: 0,
  onFinishRating: () => {},
}

export default Card
