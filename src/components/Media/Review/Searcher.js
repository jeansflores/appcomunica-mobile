import Icon from 'react-native-vector-icons/FontAwesome5'
import React, { useState } from 'react'
import styled from 'styled-components/native'
import { Platform } from 'react-native'

const Container = styled.View`
  flex: 1;
  justify-content: center;
`

const ModalSearch = styled.Modal``

const ModalContainer = styled.View`
  background-color: rgba(0, 0, 0, 0.6);
  flex: 1;
`

const InputContainer = styled.View`
  align-items: center;
  background-color: ${props => props.theme.colors.surface};
  border-radius: 5;
  flex-direction: row;
  justify-content: space-between;
  margin-horizontal: 10;
  margin-top: ${ Platform.select({ ios: 65, android: 55 }) };
  padding-horizontal: 13;
`

const ButtonBack = styled.View``

const InputSearch = styled.TextInput`
  background-color: ${props => props.theme.colors.surface};
  height: 40;
  width: 85%;
`

const ButtonClose = styled.View``

export default Searcher = ({ navigation }) => {
  const [modalVisible, setModalVisible] = useState(false)
  const [search, setSearch] = useState('')
  const [buttonClear, setButtonClear] = useState(false)

  const openModal = () => {
    setModalVisible(true)
  }

  const closeModal = () => {
    setModalVisible(false)
  }

  const clearInput = (text) => {
    if (text) {
      setButtonClear(true)
    } else {
      setButtonClear(false)
    }
  }

  return (
    <>
      <Icon
        style={{ marginRight: 20, color: '#000' }}
        name="search"
        size={20}
        onPress={() => openModal()}
      />

      <Container>
        <ModalSearch
          visible={modalVisible}
          animationType={'fade'}
          onRequestClose={() => closeModal()}
          transparent={true}
        >
          <ModalContainer>
            <InputContainer>
              <ButtonBack>
                <Icon
                  style={{color: '#555' }}
                  name="arrow-left"
                  size={20}
                  onPress={() => closeModal()}

                />
              </ButtonBack>
              <InputSearch
                placeholder="Buscar"
                value={search}
                autoFocus={true}
                onChangeText={text => {
                  setSearch(text)
                  clearInput(text)
                }}
                onSubmitEditing={() => {
                  navigation.setParams({ title: search })
                  navigation.navigate('reviews', { search: search })
                  closeModal()
                }}
              />
              <ButtonClose>
                { buttonClear &&
                  <Icon
                    style={{color: '#555' }}
                    name="times"
                    size={20}
                    onPress={() => {
                      setSearch('')
                      navigation.setParams({ title: '' })
                      setButtonClear(false)
                    }}
                  />
                }
              </ButtonClose>
            </InputContainer>
          </ModalContainer>
        </ModalSearch>
      </Container>
    </>
  )
}
