import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import { StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import Card from './Card'

const InfoParticipant = styled.View`
  flexDirection: row;
  flex: 1;
`

const Label = styled.Text`
  text-align: right;
  flex: 0.5;
  font-weight: bold;
`

const LabelParticipant = styled.Text`
  flex: 0.5;
  font-weight: bold;
  color: #52BE80;
`

const checkParticipant = (ticket) => {
  if (ticket) {
    return (
      <InfoParticipant>
        <LabelParticipant>
          Participando
        </LabelParticipant>
      </InfoParticipant>
    )
  } else {
    return (
      <InfoParticipant>
        <Icon name="info" color="grey"> Saiba mais...</Icon>
      </InfoParticipant>
    )
  }
}

const MyRafflePreview = ({ raffle, onPress }) => {
  const { ticket } = raffle

  return (
    <Card
      title={raffle.name}
      image={raffle.image}
      date={raffle.publishedAt}
      onPress={onPress}
    >
      {checkParticipant(ticket)}
      <Label style={[ raffle.status == 'Aberto' ? styles.statusOpened : '' ]}>
        {raffle.status}
      </Label>
    </Card>
  )
}

const styles = StyleSheet.create({
  statusOpened: {
    color: '#39507c',
  },
})

MyRafflePreview.propTypes = {
  raffle: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    image: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]).isRequired
  }),
  onPress: PropTypes.func
}

MyRafflePreview.defaultProps = {
  onPress: () => {}
}

export default MyRafflePreview
