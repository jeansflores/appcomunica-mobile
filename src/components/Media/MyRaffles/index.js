import { Text, View, Image, ScrollView, StyleSheet } from 'react-native'
import { theme } from '~/utils'
import Html from 'react-native-render-html'
import Moment from '~/components/Moment'
import PropTypes from 'prop-types'
import React from 'react'
import CheckTicket from './CheckTicket'
import styled from 'styled-components/native'
import Alert from '~/components/CustomAlert'

const Button = styled.TouchableOpacity`
  align-items: center;
  align-self: center;
  background-color: ${props => props.theme.colors.secondary};
  border-radius: 20;
  elevation: 1;
  height: 32;
  justify-content: center;
  padding-horizontal: 5;
  padding-vertical: 5;
  shadow-color: ${props => props.theme.colors.text};
  shadow-offset: 0px 3px;
  shadow-opacity: 0.25;
  shadow-radius: 5;
  width: 150;
`

const ButtonLabel = styled.Text`
  font-weight: bold;
`

const MyRaffles = ({ raffle, ticket, onParticipatePress, navigation }) => {
  return (
    <ScrollView style={styles.container}>
      <View style={styles.imageContainer}>
        <Image
          style={styles.image}
          source={{ uri: raffle.image }}
        />
      </View>

      <View style={styles.bodyContainer}>
        <Text style={styles.name}>{raffle.name}</Text>
        <Text style={styles.info}>
          Dia do sorteio: <Moment format="DD/MM/YYYY">{raffle.raffledAt}</Moment>
        </Text>
        <View style={styles.description}>
          <Html html={raffle.description} />
          <Button
            style={{ marginTop: 20 }}
            activeOpacity={0.7}
            onPress={() => navigation.navigate('rules', { raffle })}
          >
            <ButtonLabel>Regulamento</ButtonLabel>
          </Button>
        </View>
        { ticket &&
          <CheckTicket ticket={ticket} status={raffle.status} />
        }
        { !ticket && raffle.remainingVotes > 0 && !raffle.single && raffle.status == 'Aberto' &&
          <Alert
            message={"Você ainda não está participando deste sorteio, faltam apenas " + raffle.remainingVotes + " votos para participar"}
          />
        }
        { !ticket && (raffle.remainingVotes == 0 || raffle.single) && raffle.status == 'Aberto' &&
          <Button
            activeOpacity={0.9}
            onPress={() => onParticipatePress(raffle)}
          >
            <ButtonLabel>Quero Participar</ButtonLabel>
          </Button>
        }
      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  imageContainer: {
    flex: 0.3,
  },
  image: {
    height: 250,
  },
  bodyContainer: {
    flex: 0.7,
    paddingHorizontal: 10,
    marginBottom: 10
  },
  name: {
    fontSize: 16,
    fontWeight: 'bold',
    paddingVertical: 10,
    color: theme.colors.text,
  },
  info: {
    color: theme.colors.text,
  },
  description: {
    marginTop: 10,
    marginBottom: 20,
    color: theme.colors.text,
  }
})

MyRaffles.propTypes = {
  raffle: PropTypes.shape({
    image: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]).isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    single: PropTypes.bool.isRequired,
    publishedAt: PropTypes.string.isRequired
  }),
  ticket: PropTypes.shape({
    raffled: PropTypes.bool.isRequired,
    token: PropTypes.string.isRequired,
  })
}

export default MyRaffles
