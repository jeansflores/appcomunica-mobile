import styled from 'styled-components/native';

export const Container = styled.View`
  margin-bottom: 5;
  margin-bottom: 10;
`

export const CardParticipant = styled.View`
  flex-direction: column;
  align-items: center;
  padding-vertical: 6;
  padding-horizontal: 6;
  border-radius: 5;
  background-color: ${props => props.theme.colors.primary};
`

export const CardLose = styled.View`
  flex-direction: column;
  align-items: center;
  padding-vertical: 6;
  padding-horizontal: 6;
  border-radius: 5;
  background-color: #E74C3C;
`

export const CardWinner = styled.View`
  flex-direction: column;
  align-items: center;
  padding-vertical: 6;
  padding-horizontal: 6;
  border-radius: 5;
  background-color: #27AE60;
`

export const Token = styled.Text`
  font-size: 30;
  font-weight: bold;
  margin-top: 5;
  color: ${props => props.theme.colors.surface}
`

export const Message = styled.Text`
  font-weight: bold;
  align-self: center;
  color: ${props => props.theme.colors.surface}
`
