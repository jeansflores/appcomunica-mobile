import React from 'react'
import {
  Container,
  CardParticipant,
  CardLose,
  CardWinner,
  Token,
  Message
} from './styles'

const infoTicket = (raffled, token, status) => {
  if (raffled == false && status == 'Aberto') {
    return (
      <CardParticipant>
        <Message>Você está participando deste sorteio.</Message>
        <Message>Aguarde o resultado! #boasorte</Message>
      </CardParticipant>
    )
  } else if (raffled == false && status == 'Encerrado') {
    return (
      <CardLose>
        <Message>Não foi desta vez, boa sorte nos próximos sorteios!</Message>
      </CardLose>
    )
  } else if (raffled && status == 'Encerrado') {
    return (
      <CardWinner>
        <Message>Parabéns, você foi um dos ganhadores deste sorteio!</Message>
        <Message>Segue abaixo o código de participação:</Message>
        <Token>{token}</Token>
      </CardWinner>
    )
  }
}

const CheckTicket = ({ticket, status}) => (
  <Container>
    {infoTicket(ticket.raffled, ticket.token, status)}
  </Container>
)

export default CheckTicket
