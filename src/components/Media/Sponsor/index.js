import { FlatList } from 'react-native'
import React from 'react'
import styled from 'styled-components/native'

const Container = styled.View`
  margin-horizontal: 5;
`

const Image = styled.Image`
  border-radius: 5
  height: 80;
  width: 80;
`

const Section = styled.Text`
  font-weight: bold;
  margin-bottom: 10;
  text-align: center;
`

export default ({ sponsors }) => {
  return (
    <>
      <Section>Patrocinadores</Section>
      <FlatList
        data={sponsors}
        renderItem={({ item }) => (
          <Container>
            <Image source={{ uri: item.logo }} />
          </Container>
        )}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        keyExtractor={item => item.id.toString()}
      />
    </>
  )
}
