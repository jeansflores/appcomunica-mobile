import Card from '../../Card'
import PropTypes from 'prop-types'
import React from 'react'

const ItemPreview = ({ item, onPress }) => (
  <Card
    title={item.name}
    subtitle={item.group}
    image={item.image}
    onPress={onPress}
  />
)

ItemPreview.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string.isRequired,
    group: PropTypes.string.isRequired,
    image: PropTypes.string,
  }),
  onPress: PropTypes.func
}

export default ItemPreview
