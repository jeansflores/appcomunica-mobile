import { Text, View, Image, ScrollView, StyleSheet, Dimensions } from 'react-native'
import { theme } from '~/utils'
import Html from 'react-native-render-html'
import PropTypes from 'prop-types'
import React from 'react'

const dimensions = Dimensions.get('window')
const imageHeight = Math.round(dimensions.width * 9 / 16)
const imageWidth = dimensions.width

const Item = ({ data }) => (
  <ScrollView style={styles.container}>
    <View style={styles.imageContainer}>
      <Image
        style={styles.image}
        source={{ uri: data.image }}
      />
    </View>

    <View style={styles.bodyContainer}>
      <Text style={styles.name}>{data.name}</Text>
      <View style={styles.description}>
        <Html html={data.description} />
      </View>
    </View>
  </ScrollView>
)

const styles = StyleSheet.create({
  imageContainer: {
    flex: 0.3,
  },
  image: {
    height: imageHeight,
    width: imageWidth,
  },
  bodyContainer: {
    flex: 0.7,
    paddingHorizontal: 10,
  },
  name: {
    color: theme.colors.text,
    fontSize: 16,
    fontWeight: 'bold',
    paddingVertical: 10,
  },
  info: {
    color: theme.colors.text,
  },
  description: {
    color: theme.colors.text,
    marginTop: 10,
  }
})

Item.propTypes = {
  data: PropTypes.shape({
    image: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  }).isRequired
}

export default Item
