import { Text, View, Image, ScrollView, StyleSheet } from 'react-native'
import { theme } from '~/utils'
import Html from 'react-native-render-html'
import Moment from '~/components/Moment'
import PropTypes from 'prop-types'
import React from 'react'
import CheckCoupon from './CheckCoupon'
import styled from 'styled-components/native'

const ToParticipateButton = styled.TouchableOpacity`
  align-items: center;
  align-self: center;
  background-color: ${props => props.theme.colors.secondary};
  border-radius: 10;
  border-radius: 20;
  elevation: 1;
  height: 32;
  justify-content: center;
  padding-horizontal: 5;
  padding-vertical: 5;
  shadow-color: ${props => props.theme.colors.text};
  shadow-offset: 0px 3px;
  shadow-opacity: 0.25;
  shadow-radius: 5;
  width: 150;
`

const ToParticipateButtonLabel = styled.Text`
  font-weight: bold;
`

const MyPromotion = ({ promotion, coupon, onParticipatePress }) => {
  return (
    <ScrollView style={styles.container}>
      <View style={styles.imageContainer}>
        <Image
          style={styles.image}
          source={{ uri: promotion.image }}
        />
      </View>

      <View style={styles.bodyContainer}>
        <Text style={styles.name}>{promotion.name}</Text>
        <Text style={styles.info}>
          Patrocinador: {promotion.company.tradeName}
        </Text>
        <Text style={styles.info}>
          Válido até <Moment format="DD/MM/YYYY">{promotion.expiresAt}</Moment>
        </Text>
        <View style={styles.description}>
          <Html html={promotion.description} />
        </View>
        { coupon &&
          <CheckCoupon coupon={coupon}/>
        }
        { !coupon && promotion.status == "Aberta" &&
          <ToParticipateButton
            activeOpacity={0.9}
            onPress={() => onParticipatePress(promotion)}
          >
            <ToParticipateButtonLabel>Quero Cupom</ToParticipateButtonLabel>
          </ToParticipateButton>
        }
      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  imageContainer: {
    flex: 0.3,
  },
  image: {
    height: 250,
  },
  bodyContainer: {
    flex: 0.7,
    paddingHorizontal: 10,
    marginBottom: 10
  },
  name: {
    fontSize: 16,
    fontWeight: 'bold',
    paddingVertical: 10,
    color: theme.colors.text,
  },
  info: {
    color: theme.colors.text,
  },
  description: {
    marginTop: 10,
    marginBottom: 20,
    color: theme.colors.text,
  }
})

MyPromotion.propTypes = {
  promotion: PropTypes.shape({
    image: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]).isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    expiresAt: PropTypes.string.isRequired,
    publishedAt: PropTypes.string.isRequired
  }),
  coupon: PropTypes.shape({
    token: PropTypes.string.isRequired,
  })
}

export default MyPromotion
