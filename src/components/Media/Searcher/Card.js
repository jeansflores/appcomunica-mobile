import React from 'react'
import styled from 'styled-components/native'
import Icon from 'react-native-vector-icons/FontAwesome5'

const Container = styled.TouchableOpacity`
  background-color: ${props => props.theme.colors.surface};
  border-radius: 2;
  elevation: 3;
  flex-direction: row;
  margin-bottom: 5;
  padding-horizontal: 5;
  padding-vertical: 5;
  shadow-color: ${props => props.theme.colors.text};
  shadow-offset: 0px 1px;
  shadow-opacity: 0.2;
  shadow-radius: 5;
`

const Header = styled.View`
  flex-direction: row;
  flex: 0.4;
  max-height: 105;
  padding-right: 10;
`

const Image = styled.Image`
  flex: 1;
  height: 105;
`

const Content = styled.View`
  flex-direction: row;
  flex: 0.6;
`

const LeftBar = styled.View`
  flex-direction: column;
  flex: 1;
  height: 105;
  justify-content: space-between;
  padding-horizontal: 5;
  padding-vertical: 5;
`

const Title = styled.Text`
  flex: 0.8;
  margin-horizontal: 5;
  margin-vertical: 5;
  font-size: 15;
  align-self: center;
`

const Info = styled.View`
  flex: 0.2;
  flex-direction: row;
  justify-content: space-between;
  margin-horizontal: 5;
`

const GroupCategory = styled.View`
  flex-direction: row;
  align-items: center;
`

const IconCategory = styled(Icon)`
  margin-right: 5;
`

const Category = styled.Text`
  font-size: 13;
  align-self: flex-start;
  font-style: italic;
`

const Status = styled.Text`
  align-self: flex-end;
  font-size: 13;
  font-weight: bold;
`

const ViewTitle = styled.View`
  flex: 1;
  flex-direction: row;
`

const renderInfoCategory = (category) => {
  switch (category) {
    case 'raffle':
      return (
        <GroupCategory>
          <IconCategory name='ticket-alt' size={11} />
          <Category>Sorteio</Category>
        </GroupCategory>
      )
    case 'promotion':
      return (
        <GroupCategory>
          <IconCategory name='tags' size={11} />
          <Category>Promoção</Category>
        </GroupCategory>
      )
    case 'post':
      return (
        <GroupCategory>
          <IconCategory name='newspaper' size={11} />
          <Category>Notícia</Category>
        </GroupCategory>
      )
    case 'event':
      return (
        <GroupCategory>
          <IconCategory name='calendar-alt' size={11} />
          <Category>Evento</Category>
        </GroupCategory>
      )
  }
}

const Card = ({ title, status, category, image, onPress }) => {
  return (
    <Container activeOpacity={0.75} onPress={onPress}>
      <Header>
        <Image
          source={ typeof image === 'string' ? { uri: image } : image}
          resizeMode="cover"
        />
      </Header>

      <Content>
        <LeftBar>
          <ViewTitle>
            <Title
              numberOfLines={3}
            >
              {title}
            </Title>
          </ViewTitle>
          <Info>
            {renderInfoCategory(category)}
            {status && category !== 'event' && (
              <Status style={{ color: status == 'Aberto' || status == 'Aberta' ? '#39507c' : 'grey' }}>{status}</Status>
            )}
          </Info>
        </LeftBar>
      </Content>
    </Container>
  )
}

export default Card
