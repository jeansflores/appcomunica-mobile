import Icon from 'react-native-vector-icons/FontAwesome5'
import React, { useState } from 'react'
import styled from 'styled-components/native'
import Divider from '~/components/Divider'
import { theme } from '~/utils'

const Container = styled.View`
  flex: 1;
  justify-content: center;
`

const ModalSearch = styled.Modal``

const ModalContainer = styled.View`
  flex: 1;
  background-color: rgba(0, 0, 0, 0.6);
`

const FormContainer = styled.View`
  background-color: ${props => props.theme.colors.surface};
  margin-top: 10;
  margin-horizontal: 10;
  border-radius: 5;
  padding-horizontal: 13;
  padding-vertical: 10;
`

const InputContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`

const ButtonBack = styled.View``

const InputSearch = styled.TextInput`
  height: 40;
  width: 85%;
  background-color: ${props => props.theme.colors.surface};
`

const ButtonClose = styled.View``

const CategoryContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`

const LabelCategory = styled.Text``

const ButtonCategory = styled.View`
  flex: 2.5;
  align-items: center;
  width: 100%;
  justify-content: center;
`

const IconCategory = styled(Icon)`
  padding-horizontal: 25;
  padding-vertical: 3;
`

export default Search = ({ navigation }) => {
  const [modalVisible, setModalVisible] = useState(false)
  const [search, setSearch] = useState('')
  const [buttonClear, setButtonClear] = useState(false)
  const [categoryActive, setCategoryActive] = useState('raffle')

  const openModal = () => {
    setModalVisible(true)
  }

  const closeModal = () => {
    setModalVisible(false)
  }

  const clearInput = (text) => {
    if (text) {
      setButtonClear(true)
    } else {
      setButtonClear(false)
    }
  }

  const checkColorTabActive = (category) => (
    categoryActive === category ? theme.colors.primary : '#555'
  )

  const renderButtonCategory = (category, icon, label) => (
    <ButtonCategory>
      <IconCategory
        style={{color: checkColorTabActive(category)}}
        onPress={() => setCategoryActive(category)}
        name={icon}
        size={23}
      />
      <LabelCategory
        style={{color: checkColorTabActive(category)}}
      >
        {label}
      </LabelCategory>
    </ButtonCategory>
  )

  return (
    <>
      <Icon
        style={{ color: '#FFF', paddingRight: 10, paddingLeft: 10, paddingVertical: 15, marginRight: 10 }}
        name="search"
        size={20}
        onPress={() => openModal()}
      />

      <Container>
        <ModalSearch
          visible={modalVisible}
          animationType={'fade'}
          onRequestClose={() => closeModal()}
          transparent={true}
        >
          <ModalContainer>
            <FormContainer>
              <InputContainer>
                <ButtonBack>
                  <Icon
                    style={{color: '#555'}}
                    name="arrow-left"
                    size={20}
                    onPress={() => {
                      setSearch('')
                      setCategoryActive('raffle')
                      closeModal()
                    }}
                  />
                </ButtonBack>
                <InputSearch
                  placeholder="Buscar"
                  value={search}
                  autoFocus={true}
                  onChangeText={text => {
                    setSearch(text)
                    clearInput(text)
                  }}
                  onSubmitEditing={() => {
                    if (search) {
                      let filterClosed = navigation.getParam('filterClosed') || false
                      let filterCategory = navigation.getParam('category')

                      if (filterCategory !== 'event' && filterCategory !== 'post') {
                        filterClosed = false
                      }

                      setSearch('')
                      setCategoryActive('raffle')
                      navigation.navigate(
                        {
                          routeName: 'searcher',
                          params: { search, category: categoryActive, filterClosed },
                          key: search + '-' + categoryActive + '-' + filterClosed
                        }
                      )
                      closeModal()
                    }
                  }}
                />
                <ButtonClose>
                  { buttonClear &&
                    <Icon
                      style={{color: '#555'}}
                      name="times"
                      size={20}
                      onPress={() => {
                        setSearch('')
                        setButtonClear(false)
                      }}
                    />
                  }
                </ButtonClose>
              </InputContainer>
              <Divider />
              <CategoryContainer>
                {renderButtonCategory('raffle', 'ticket-alt', 'Sorteios')}
                {renderButtonCategory('promotion', 'tags', 'Promoções')}
                {renderButtonCategory('post', 'newspaper', 'Notícias')}
                {renderButtonCategory('event', 'calendar-alt', 'Eventos')}
              </CategoryContainer>
            </FormContainer>
          </ModalContainer>
        </ModalSearch>
      </Container>
    </>
  )
}
