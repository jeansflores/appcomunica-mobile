import Icon from 'react-native-vector-icons/FontAwesome5'
import React, { useState } from 'react'
import styled from 'styled-components/native'
import color from 'color'
import { theme } from '~/utils'
import { Platform } from 'react-native'

const Container = styled.View`
  flex: 1;
  justify-content: center;
`

const ModalSearch = styled.Modal``

const ModalContainer = styled.View`
  flex: 1;
  background-color: rgba(0, 0, 0, 0.6);
`

const FormContainer = styled.View`
  background-color: ${props => props.theme.colors.surface};
  margin-top: 10;
  margin-horizontal: 10;
  border-radius: 5;
  padding-horizontal: 13;
  padding-vertical: 10;
  height: 130;
`

const TitleGroupFilter = styled.Text`
  margin-bottom: 5;
  font-size: 16;
`

const GroupInputs = styled.View`
  flex: 1;
  justify-content: space-between;
  flex-direction: row;
  padding-vertical: 10;
`

const LabelField = styled.View``
const InputField = styled.View``

const Label = styled.Text`
  font-size: 15;
`
const Switch = styled.Switch`
  color: red;
`

const Footer = styled.View`
  flex: 1;
  margin-top: 15;
  flex-direction: row;
  align-self: flex-end;
`

const ActionFooter = styled.Text`
  font-weight: bold;
  color: ${theme.colors.primary};
`

export default Filter = ({ navigation }) => {
  const [modalVisible, setModalVisible] = useState(false)
  const [showClosed, setShowClosed] = useState(navigation.getParam('filterClosed') || false)

  const openModal = () => {
    setModalVisible(true)
  }

  const closeModal = () => {
    setModalVisible(false)
  }

  return (
    <>
      <Icon
        style={{ color: '#FFF', paddingRight: 10, paddingLeft: 10, paddingVertical: 15, marginRight: 10 }}
        name="filter"
        size={20}
        onPress={() => openModal()}
      />

      <Container>
        <ModalSearch
          visible={modalVisible}
          animationType={'fade'}
          onRequestClose={() => closeModal()}
          transparent={true}
        >
          <ModalContainer>
            <FormContainer>
              <TitleGroupFilter>
                OPÇÕES DE FILTRO
              </TitleGroupFilter>
              <GroupInputs>
                <LabelField>
                  <Label>Mostrar encerradas</Label>
                </LabelField>
                <InputField>
                  <Switch
                    onValueChange={value => setShowClosed(value)}
                    trackColor={{
                      true: color(theme.colors.primary).lighten(1),
                      false: Platform.OS=='android'?'#d3d3d3':'#fbfbfb'
                    }}
                    thumbColor={[
                      Platform.OS=='ios'
                      ? theme.colors.surface
                      : (showClosed ? theme.colors.primary : theme.colors.surface)
                    ]}
                    ios_backgroundColor="#fbfbfb"
                    value={showClosed}
                  />
                </InputField>
              </GroupInputs>
              <Footer>
                <ActionFooter onPress={() => closeModal()}>
                  CANCELAR
                </ActionFooter>
                <ActionFooter
                  style={{marginLeft: 20}}
                  onPress={() => {
                    navigation.navigate(
                      {
                        routeName: 'searcher',
                        params: { ...navigation.state.params, filterClosed: showClosed },
                        key: navigation.getParam('search') + '-' + navigation.getParam('category') + '-' + Math.random()
                      }
                    )
                    closeModal()
                  }}
                >
                  APLICAR
                </ActionFooter>
              </Footer>
            </FormContainer>
          </ModalContainer>
        </ModalSearch>
      </Container>
    </>
  )
}
