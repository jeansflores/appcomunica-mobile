import 'moment/locale/pt-br'
import { Platform } from 'react-native'
import { timeAgo } from '~/utils'
import moment from 'moment'
import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components/native'

const Container = styled.TouchableOpacity`
  background-color: ${props => props.theme.colors.surface};
  border-radius: 2;
  elevation: 3;
  flex-direction: column;
  margin-bottom: 5;
  shadow-color: ${props => props.theme.colors.text};
  shadow-offset: 0px 1px;
  shadow-opacity: 0.2;
  shadow-radius: 5;
`

const Header = styled.View`
  flex-direction: row;
  flex: 1;
`

const Image = styled.Image`
  height: 200;
  width: 100%;
`

const Content = styled.View`
  flex-direction: column;
  flex: 1;
  justify-content: space-between;
  margin-horizontal: 10;
  margin-vertical: 10;
`

const ContentHeader = styled.View`
  flex-direction: row;
  flex: 0.5;
  justify-content: space-between;
  padding-bottom: ${ Platform.select({ ios: 10, android: 15 }) };
`

const ContentFooter = styled.View`
  bottom: 0;
  flex-direction: row;
  flex: 0.5;
  justify-content: space-between;
`

const LeftBar = styled.View`
  flex: 0.9;
`

const RightBar = styled.View`
  flex: 0.1;
`

const Title = styled.Text`
  font-size: 15;
  font-weight: bold;
`

const Time = styled.Text`
  color: #777;
  font-size: 10;
  text-align: right;
`

const Card = ({ title, children, date, image, onPress }) => (
  <Container activeOpacity={0.75} onPress={onPress}>
    <Header>
      <Image
        source={ typeof image === 'string' ? { uri: image } : image }
        resizeMode="cover"
      />
    </Header>
    <Content>
      <ContentHeader>
        <LeftBar>
          <Title>{title}</Title>
        </LeftBar>

        <RightBar>
          <Time>{timeAgo(moment(date).format("x"))}</Time>
        </RightBar>
      </ContentHeader>

      <ContentFooter>
        {children}
      </ContentFooter>
    </Content>
  </Container>
)

Card.propTypes = {
  title: PropTypes.string.isRequired,
  image: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired,
  onPress: PropTypes.func
}

Card.defaultProps = {
  onPress: () => {}
}

export default Card
