import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import { StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import Card from './Card'

const InfoParticipant = styled.View`
  flexDirection: row;
  flex: 1;
`

const Label = styled.Text`
  text-align: right;
  flex: 0.5;
  font-weight: bold;
`

const LabelParticipant = styled.Text`
  flex: 0.5;
  font-weight: bold;
  color: #52BE80;
`

const checkParticipant = (coupon) => {
  if (coupon) {
    return (
      <InfoParticipant>
        <LabelParticipant>
          Já pegou
        </LabelParticipant>
      </InfoParticipant>
    )
  } else {
    return (
      <InfoParticipant>
        <Icon name="info" color="grey"> Saiba mais...</Icon>
      </InfoParticipant>
    )
  }
}

const PromotionPreview = ({ promotion, onPress }) => {
  const { coupon } = promotion

  return (
    <Card
      title={promotion.name}
      image={promotion.image}
      date={promotion.publishedAt}
      onPress={onPress}
    >
      {checkParticipant(coupon)}
      <Label style={[promotion.status == 'Aberta' && promotion.hasCoupons ? styles.statusOpened : '' ]}>
        {promotion.hasCoupons ? promotion.status : 'Encerrada'}
      </Label>
    </Card>
  )
}

const styles = StyleSheet.create({
  statusOpened: {
    color: '#39507c',
  },
})

PromotionPreview.propTypes = {
  promotion: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    image: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]).isRequired
  }),
  onPress: PropTypes.func
}

PromotionPreview.defaultProps = {
  onPress: () => {}
}

export default PromotionPreview
