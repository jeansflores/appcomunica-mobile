import React from 'react'
import {
  Container,
  CardParticipant,
  Token,
  Message
} from './styles'

const CheckCoupon = ({coupon}) => (
  <Container>
    <CardParticipant>
      <Message>Cupom Garantido!</Message>
      <Message>Vá até o estabelecimento e troque seu cupom.</Message>
      <Message>Segue abaixo o código de participação:</Message>
      <Token>{coupon.token}</Token>
    </CardParticipant>
  </Container>
)

export default CheckCoupon
