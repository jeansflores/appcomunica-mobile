import React from 'react'
import PropTypes from 'prop-types'
import Html from 'react-native-render-html'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { Text, View, Image, ScrollView, StyleSheet } from 'react-native'

import Moment from '../../Moment'
import { theme } from '~/utils'

const Post = ({ post }) => (
  <ScrollView style={styles.container}>
    <View style={styles.imageContainer}>
      <Image
        style={styles.image}
        source={{ uri: post.image }}
      />
    </View>

    <View style={styles.bodyContainer}>
      <Text style={styles.title}>{post.title}</Text>
      <Text style={styles.info}>
          Postado em <Moment format="DD/MM/YYYY">{post.publishedAt}</Moment>
      </Text>
      <View style={styles.body}>
        <Html html={post.content} />
      </View>
    </View>
  </ScrollView>
)

const styles = StyleSheet.create({
  imageContainer: {
    flex: 0.3,
  },
  image: {
    height: 250,
  },
  bodyContainer: {
    flex: 0.7,
    paddingHorizontal: 10,
  },
  title: {
    color: theme.colors.text,
    fontSize: 16,
    fontWeight: 'bold',
    paddingVertical: 10,
  },
  info: {
    color: theme.colors.text,
  },
  body: {
    marginTop: 10,
    color: theme.colors.text,
  }
})

Post.propTypes = {
  post: PropTypes.shape({
    image: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]).isRequired,
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    publishedAt: PropTypes.string.isRequired
  })
}

export default Post
