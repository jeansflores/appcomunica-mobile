import { sliceText } from '~/utils'
import Card from '../../Card'
import PropTypes from 'prop-types'
import React from 'react'

const PostPreview = ({ post, onPress }) => (
  <Card
    title={sliceText(post.title, 90)}
    image={post.image}
    onPress={onPress}
  />
)

PostPreview.propTypes = {
  post: PropTypes.shape({
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    image: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]).isRequired
  }),
  onPress: PropTypes.func
}

export default PostPreview
