import { sliceText } from '~/utils'
import Card from '../../Card'
import Moment from '../../Moment'
import PropTypes from 'prop-types'
import React from 'react'

const EventPreview = ({ event, onPress }) => (
  <Card
    title={sliceText(event.name, 60)}
    subtitle={<Moment format="LLL">{event.startAt}</Moment>}
    image={event.image}
    onPress={onPress}
  />
)

EventPreview.propTypes = {
  event: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    image: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]).isRequired
  }),
  onPress: PropTypes.func
}

export default EventPreview
