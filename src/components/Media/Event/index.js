import { Text, View, Image, ScrollView, StyleSheet, Linking } from 'react-native'
import { theme } from '~/utils'
import Html from 'react-native-render-html'
import Icon from 'react-native-vector-icons/FontAwesome5'
import Moment from '../../Moment'
import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components/native'

const ButtonContainer = styled.View`
  flex-direction: row;
  flex: 1;
  justify-content: center;
  margin-bottom: 10;
`

const Button = styled.TouchableOpacity`
  align-items: center;
  align-self: center;
  background-color: ${props => props.theme.colors.secondary};
  border-radius: 20;
  elevation: 1;
  height: 32;
  justify-content: center;
  margin-bottom: 5;
  margin-horizontal: 5;
  margin-top: 15;
  padding-horizontal: 5;
  padding-vertical: 5;
  shadow-color: ${props => props.theme.colors.text};
  shadow-offset: 0px 3px;
  shadow-opacity: 0.25;
  shadow-radius: 5;
  width: 110;
`

const Event = ({ event }) => (
  <ScrollView style={styles.container}>
    <View style={styles.imageContainer}>
      <Image
        style={styles.image}
        source={{ uri: event.image }}
      />
    </View>

    <View style={styles.bodyContainer}>
      <Text style={styles.title}>{event.name}</Text>
      <Text style={styles.info}>
        Dia do evento: <Moment format="LLL">{event.startAt}</Moment>
      </Text>
      <View style={styles.body}>
        <Html html={event.description} />

        <ButtonContainer>
          { event.mapUrl != "" &&
            <Button
              activeOpacity={0.7}
              onPress={() => Linking.openURL(event.mapUrl)}
            >
              <Text style={{ alignItems: 'center' }}>
                <Icon name="map-marker" /> <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>Local</Text>
              </Text>
            </Button>
          }

          { event.link != "" &&
            <Button
              activeOpacity={0.7}
              onPress={() => Linking.openURL(event.link)}
            >
              <Text style={{ alignItems: 'center' }}>
                <Icon name="link" /> <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>Site</Text>
              </Text>
            </Button>
          }
        </ButtonContainer>
      </View>
    </View>
  </ScrollView>
)

const styles = StyleSheet.create({
  imageContainer: {
    flex: 0.3,
  },
  image: {
    height: 250,
  },
  bodyContainer: {
    flex: 0.7,
    paddingHorizontal: 10,
  },
  title: {
    color: theme.colors.text,
    fontSize: 16,
    fontWeight: 'bold',
    paddingVertical: 10,
  },
  info: {
    color: theme.colors.text,
  },
  body: {
    color: theme.colors.text,
    marginTop: 10,
  }
})

Event.propTypes = {
  event: PropTypes.shape({
    image: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]).isRequired,
    name: PropTypes.string.isRequired,
    mapUrl: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    publishedAt: PropTypes.string.isRequired
  })
}

export default Event
